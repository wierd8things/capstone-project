package com.epam.pcconfig.repositories;

import com.epam.pcconfig.models.ProductAttribute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductAttributeRepository extends JpaRepository<ProductAttribute, Long> {
    Optional<ProductAttribute> findByAttributeName(String attributeName);

    ProductAttribute findProductAttributeByAttributeName(String attributeName);
}
