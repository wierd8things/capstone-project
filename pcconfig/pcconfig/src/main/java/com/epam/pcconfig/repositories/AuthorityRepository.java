package com.epam.pcconfig.repositories;

import com.epam.pcconfig.models.Authority;
import com.epam.pcconfig.roles.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    Authority findByUserUserId(Long userId);

    boolean existsByUser_UserIdAndAuthority(Long userId, Role authority);

    boolean existsByAuthority(Role authority);
}
