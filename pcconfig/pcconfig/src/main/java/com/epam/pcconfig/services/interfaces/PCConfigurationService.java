package com.epam.pcconfig.services.interfaces;

import com.epam.pcconfig.models.PCConfiguration;
import com.epam.pcconfig.models.PCConfigurationProduct;
import com.epam.pcconfig.models.dto.pcconfigs.PCConfigDto;
import com.epam.pcconfig.roles.Role;

import java.util.List;

public interface PCConfigurationService {
    List<PCConfiguration> getPCConfigsWhereUserIsSeller(Role role);

    PCConfiguration postNewCustomPC(PCConfigDto pcConfigDto, String username);

    PCConfigurationProduct assignProductToPc(String username, Long productId);

    void saveCustomConfig(PCConfiguration pcConfiguration);

    List<PCConfiguration> getPCConfigMadeByUser(String username);

    List<PCConfiguration> getAllPCConfigs();
}
