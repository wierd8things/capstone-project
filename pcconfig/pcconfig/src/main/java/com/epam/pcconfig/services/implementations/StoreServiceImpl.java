package com.epam.pcconfig.services.implementations;

import com.epam.pcconfig.models.Store;
import com.epam.pcconfig.repositories.StoreRepository;
import com.epam.pcconfig.services.interfaces.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StoreServiceImpl implements StoreService {
    @Autowired
    private final StoreRepository storeRepository;

    public StoreServiceImpl(StoreRepository storeRepository) {
        this.storeRepository = storeRepository;
    }

    @Override
    public Store findStoreByUsername(String username) {
        return storeRepository.findStoreByUserUsername(username);
    }
}
