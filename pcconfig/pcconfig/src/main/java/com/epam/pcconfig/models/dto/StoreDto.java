package com.epam.pcconfig.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreDto {
    private String username;
    private String storeName;
    private String address;
    private String contactEmail;
    private String status;
}
