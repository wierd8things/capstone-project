package com.epam.pcconfig.models.dto;

import com.epam.pcconfig.models.Authority;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserDto {
    private Long userId;
    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private List<String> roles;
    private boolean enabled;
}
