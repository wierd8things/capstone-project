package com.epam.pcconfig.models.dto.products;

public enum ProductTypeEnum {
    CPU,
    GPU,
    RAM,
    SSD,
    HDD,
    MOTHERBOARD,
    COOLING_SYSTEM,
    POWER_SUPPLY
}
