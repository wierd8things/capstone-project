package com.epam.pcconfig.controllers;

import com.epam.pcconfig.models.User;
import com.epam.pcconfig.models.dto.UserRegisterDto;
import com.epam.pcconfig.services.implementations.UserServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/signin")
public class SignInController {
    private final UserServiceImpl userService;

    public SignInController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @GetMapping()
    public String showRegistrationFrom(Model model) {
        UserRegisterDto userRegisterDto = new UserRegisterDto();
        model.addAttribute("user", userRegisterDto);

        return "/signorlogin/signin";
    }

    @PostMapping("/save")
    public String registerUserAccount(@ModelAttribute("user") UserRegisterDto userRegisterDto,
                                      BindingResult bindingResult,
                                      Model model) {
        User existingUser = userService.findByUserEmail(userRegisterDto.getEmail());

        if(existingUser != null && existingUser.getContactInfo().getEmail() != null && !existingUser.getContactInfo().getEmail().isEmpty()){
            bindingResult.rejectValue("email", "error.email",
                    "There is already an account registered with the same email");
        }

        if(bindingResult.hasErrors()){
            model.addAttribute("user", userRegisterDto);
            return "signorlogin/signin";
        }

        userService.registerUser(userRegisterDto);

        return "/authorized/success";
    }

    private UserRegisterDto mapper(User user) {
        UserRegisterDto userRegisterDto = new UserRegisterDto();
        userRegisterDto.setUsername(user.getUsername());
        userRegisterDto.setFirstName(user.getContactInfo().getFirst_name());
        userRegisterDto.setLastName(user.getContactInfo().getLast_name());
        userRegisterDto.setEmail(user.getContactInfo().getEmail());
        userRegisterDto.setPassword(user.getPassword());

        return userRegisterDto;
    }
}
