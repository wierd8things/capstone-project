package com.epam.pcconfig.models.dto.products;

public enum MeasureUnitEnum {
    GB,
    MHZ,
    GHZ,
    MB,
    BIT,
    W,
    PCS,
    MM,
    MBPS,
    NM,
    PIN,
    TB,
    V,
    RPM,
    INCHES
}
