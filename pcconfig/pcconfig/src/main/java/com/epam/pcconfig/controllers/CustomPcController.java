package com.epam.pcconfig.controllers;

import com.epam.pcconfig.models.PCConfiguration;
import com.epam.pcconfig.models.PCConfigurationProduct;
import com.epam.pcconfig.models.dto.pcconfigs.PCConfigDto;
import com.epam.pcconfig.models.dto.products.ProductTypeEnum;
import com.epam.pcconfig.services.implementations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/custompc")
public class CustomPcController {

    private final PCConfigurationServiceImpl pcConfigurationService;
    private final ProductServiceImpl productService;
    private final PCConfigurationProductServiceImpl pcConfigurationProductService;

    @Autowired
    public CustomPcController(PCConfigurationServiceImpl pcConfigurationService, ProductServiceImpl productService, PCConfigurationProductServiceImpl pcConfigurationProductService) {
        this.pcConfigurationService = pcConfigurationService;
        this.productService = productService;
        this.pcConfigurationProductService = pcConfigurationProductService;
    }

    @GetMapping("/build")
    public String customPcBuilder(Model model) {
        model.addAttribute("custompc", new PCConfiguration());

        return "custompc/createpc";
    }

    @PostMapping("/newpc")
    public String addNewPc(@ModelAttribute("custompc") PCConfigDto pcConfigDto,
                           RedirectAttributes redirectAttributes) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        redirectAttributes.addFlashAttribute("saveMessage", "Custom PC saved");

        pcConfigurationService.postNewCustomPC(pcConfigDto, username);

        return "redirect:/custompc/addcpuform";
    }

    @GetMapping("/addcpuform")
    public String addCpuForm(Model model) {
        model.addAttribute("cpu", productService.getAllProductsByTypeName(ProductTypeEnum.CPU));

        return "custompc/postnewcpu";
    }

    @PostMapping("/postcputopc/{productId}")
    public String addCpu(@PathVariable("productId") Long productId, RedirectAttributes redirectAttributes) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        redirectAttributes.addFlashAttribute("saveMessage", "CPU added to your PC");


        pcConfigurationService.assignProductToPc(username, productId);

        return "redirect:/custompc/addgpuform";
    }

    @GetMapping("/addgpuform")
    public String addGpuForm(Model model) {
        model.addAttribute("gpu", productService.getAllProductsByTypeName(ProductTypeEnum.GPU));

        return "custompc/postnewgpu";
    }

    @PostMapping("/postgputopc")
    public String addGpu(@RequestParam("productId") Long productId, RedirectAttributes redirectAttributes) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        redirectAttributes.addFlashAttribute("saveMessage", "RAM added to your PC");

        pcConfigurationService.assignProductToPc(username, productId);

        return "redirect:/custompc/addramform";
    }

    @GetMapping("/addramform")
    public String addRamForm(Model model) {
        model.addAttribute("ram", productService.getAllProductsByTypeName(ProductTypeEnum.RAM));

        return "custompc/postnewram";
    }

    @PostMapping("/postramtopc")
    public String addRam(@RequestParam("productId") Long productId, RedirectAttributes redirectAttributes) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        redirectAttributes.addFlashAttribute("saveMessage", "RAM added to your PC");

        pcConfigurationService.assignProductToPc(username, productId);

        return "redirect:/custompc/addssdform";
    }

    @GetMapping("/addssdform")
    public String addSsdForm(Model model) {
        model.addAttribute("ssd", productService.getAllProductsByTypeName(ProductTypeEnum.SSD));

        return "custompc/postnewssd";
    }

    @PostMapping("/postssdtopc")
    public String addSsd(@RequestParam("productId") Long productId, RedirectAttributes redirectAttributes) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        redirectAttributes.addFlashAttribute("saveMessage", "SSD added to your PC");

        pcConfigurationService.assignProductToPc(username, productId);

        return "redirect:/custompc/addhddform";
    }

    @GetMapping("/addhddform")
    public String addHddForm(Model model) {
        model.addAttribute("hdd", productService.getAllProductsByTypeName(ProductTypeEnum.HDD));

        return "custompc/postnewhdd";
    }

    @PostMapping("/posthddtopc")
    public String addHdd(@RequestParam("productId") Long productId, RedirectAttributes redirectAttributes) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        redirectAttributes.addFlashAttribute("saveMessage", "HDD added to your PC");

        pcConfigurationService.assignProductToPc(username, productId);

        return "redirect:/custompc/addmotherboardform";
    }

    @GetMapping("/addmotherboardform")
    public String addMotherboardForm(Model model) {
        model.addAttribute("motherboard", productService.getAllProductsByTypeName(ProductTypeEnum.MOTHERBOARD));

        return "custompc/postnewmotherboard";
    }

    @PostMapping("/postmotherboardtopc")
    public String addMotherboard(@RequestParam("productId") Long productId, RedirectAttributes redirectAttributes) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        redirectAttributes.addFlashAttribute("saveMessage", "Motherboard added to your PC");

        pcConfigurationService.assignProductToPc(username, productId);

        return "redirect:/custompc/addcoolingform";
    }

    @GetMapping("/addcoolingform")
    public String addCoolingForm(Model model) {
        model.addAttribute("cooling", productService.getAllProductsByTypeName(ProductTypeEnum.COOLING_SYSTEM));

        return "custompc/postnewcooling";
    }

    @PostMapping("/postcoolingtopc")
    public String addCooling(@RequestParam("productId") Long productId, RedirectAttributes redirectAttributes) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        redirectAttributes.addFlashAttribute("saveMessage", "Cooling system added to your PC");

        pcConfigurationService.assignProductToPc(username, productId);

        return "redirect:/custompc/addpowerform";
    }

    @GetMapping("/addpowerform")
    public String addPowerForm(Model model) {
        model.addAttribute("power", productService.getAllProductsByTypeName(ProductTypeEnum.POWER_SUPPLY));

        return "custompc/postnewpower";
    }

    @PostMapping("/postpowertopc")
    public String addPower(@RequestParam("productId") Long productId, RedirectAttributes redirectAttributes) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        redirectAttributes.addFlashAttribute("saveMessage", "Power supply unit added to your PC");

        pcConfigurationService.assignProductToPc(username, productId);

        return "redirect:/custompc/saved";
    }

    @GetMapping("/saved")
    public String getSavedCustomConfigs(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        List<PCConfiguration> userCustomPc = pcConfigurationService.getPCConfigMadeByUser(username);

        Map<Long, List<PCConfigurationProduct>> configProductsMap = new HashMap<>();
        for (PCConfiguration config : userCustomPc) {
            List<PCConfigurationProduct> products = pcConfigurationProductService.getAllProductsByConfigId(config.getConfigId());
            configProductsMap.put(config.getConfigId(), products);
        }

        model.addAttribute("configProductsMap", configProductsMap);
        model.addAttribute("userConfig", userCustomPc);

        return "custompc/saved";
    }


}
