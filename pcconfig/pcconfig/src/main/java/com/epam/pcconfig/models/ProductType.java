package com.epam.pcconfig.models;

import com.epam.pcconfig.models.dto.products.ProductTypeEnum;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product_types")
public class ProductType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_type_id")
    private Long productTypeId;

    @Enumerated(EnumType.STRING)
    @Column(name = "product_type_name")
    private ProductTypeEnum productTypeName;

    @OneToMany(mappedBy = "productType")
    private Set<Product> product;
}
