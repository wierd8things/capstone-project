package com.epam.pcconfig.models;

import com.epam.pcconfig.models.dto.products.MeasureUnitEnum;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product_attributes")
public class ProductAttribute {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_attribute_id")
    private Long productAttributeId;

    @Column(name = "attribute_name")
    private String attributeName;

    @Enumerated(EnumType.STRING)
    @Column(name = "measurement_unit")
    private MeasureUnitEnum measurementUnit;

    @OneToMany(mappedBy = "productAttribute")
    private List<ProductAttributeValue> productAttributeValues;
}
