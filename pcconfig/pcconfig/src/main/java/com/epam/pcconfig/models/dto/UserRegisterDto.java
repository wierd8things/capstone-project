package com.epam.pcconfig.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRegisterDto {
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}
