package com.epam.pcconfig.repositories;

import com.epam.pcconfig.models.Product;
import com.epam.pcconfig.models.ProductToStore;
import com.epam.pcconfig.models.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface ProductToStoreRepository extends JpaRepository<ProductToStore, Long> {
    List<ProductToStore> findByProduct_ProductType(ProductType productType);

    @Query("SELECT MIN(pts.storePrice) FROM ProductToStore pts WHERE pts.product.productId = :productId")
    BigDecimal findMinPriceByProductId(@Param("productId") Long productId);

    @Query("SELECT pts.product FROM ProductToStore pts WHERE pts.store.storeId = :storeId")
    List<Product> findByStoreId(@Param("storeId") Long storeId);
}
