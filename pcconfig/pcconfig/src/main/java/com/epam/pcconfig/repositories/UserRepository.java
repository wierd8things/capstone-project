package com.epam.pcconfig.repositories;

import com.epam.pcconfig.models.User;
import com.epam.pcconfig.roles.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT u FROM User u LEFT JOIN FETCH u.authorities WHERE u.username = :username")
    Optional<User> findByUsernameWithAuthorities(@Param("username") String username);

    User findByContactInfo_Email(String email);

    User findByUsername(String username);

    List<User> findByAuthorities_Authority(Role authority);

    Optional<User> findByUserId(Long userId);

    User findUserByUsernameAndAuthorities_Authority(String username, Role authority);
}
