package com.epam.pcconfig.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "pc_configurations")
public class PCConfiguration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "config_id")
    private Long configId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    private User user;

    @Column(name = "config_name")
    private String configName;

    @Column(name = "is_custom")
    private Boolean isCustom;

    @Column(name = "create_date")
    private LocalDate createDate;

    @OneToMany(mappedBy = "pcConfiguration")
    private List<PCConfigurationProduct> pcConfigurationProducts;

    public void setCustom(boolean custom) {
        this.isCustom = custom;
    }
}
