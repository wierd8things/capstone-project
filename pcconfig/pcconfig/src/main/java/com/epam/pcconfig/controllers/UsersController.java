package com.epam.pcconfig.controllers;

import com.epam.pcconfig.models.User;
import com.epam.pcconfig.models.dto.UserDto;
import com.epam.pcconfig.services.implementations.UserServiceImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/users")
public class UsersController {
    private final UserServiceImpl userService;

    public UsersController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @GetMapping("/list")
    public String getAllUsers(Model model) {
        List<UserDto> users = userService.getAllUsers();
        model.addAttribute("users", users);

        return "/admin/users";
    }

    @PostMapping("/ban/{userId}")
    public String banUser(@PathVariable Long userId,
                          RedirectAttributes redirectAttributes) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        User user = userService.findByUsername(username);
        if (userService.findByUserId(userId) == user) {
            redirectAttributes.addFlashAttribute("warningMessage", "You can't ban yourself. That doesn't even make sense mate!");
        } else {
            userService.banUser(userId);
            redirectAttributes.addFlashAttribute("deleteMessage", "User has been banned successfully");

            return "redirect:/users/list";
        }
        return "redirect:/users/list";
    }
}
