package com.epam.pcconfig.configurations;

import com.epam.pcconfig.roles.Role;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Collection;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) authentication.getAuthorities();
        String redirectUrl = "/authorized"; // Default redirect URL

        if (authorities.contains(new SimpleGrantedAuthority(String.valueOf(Role.ADMIN)))) {
            redirectUrl = "/admin";
        } else if (authorities.contains(new SimpleGrantedAuthority(String.valueOf(Role.SELLER)))) {
            redirectUrl = "/seller/success"; // Change to your user home URL
        } else if (authorities.contains(new SimpleGrantedAuthority(String.valueOf(Role.USER)))) {
            redirectUrl = "/authorized"; // Change to your seller home URL
        }

        response.sendRedirect(redirectUrl);
    }
}
