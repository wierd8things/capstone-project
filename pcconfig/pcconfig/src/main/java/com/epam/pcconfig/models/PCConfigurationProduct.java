package com.epam.pcconfig.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "pc_configuration_products")
public class PCConfigurationProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "config_product_id")
    private Long configProductId;

    @ManyToOne
    @JoinColumn(name = "config_id", referencedColumnName = "config_id", nullable = false)
    private PCConfiguration pcConfiguration;

    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "product_id", nullable = false)
    private Product product;

    @Column(name = "min_price")
    private BigDecimal minPrice;
}
