package com.epam.pcconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PcconfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(PcconfigApplication.class, args);
	}

}
