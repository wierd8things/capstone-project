package com.epam.pcconfig.configurations;

import com.epam.pcconfig.roles.Role;
import com.epam.pcconfig.services.implementations.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Autowired
    private CustomAuthenticationSuccessHandler successHandler;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .csrf(csrf -> csrf
                        .ignoringRequestMatchers("/no-csrf"))

                .authorizeHttpRequests(authorize -> authorize
                        .requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll() // Разрешить доступ к статическим ресурсам
                        .requestMatchers("/signin/**").permitAll()
                        .requestMatchers("/index").permitAll()
                        .requestMatchers("/").permitAll()  // Добавляем доступ к корневому URL

                        .requestMatchers("/authorized").hasAnyAuthority(String.valueOf(Role.USER), String.valueOf(Role.ADMIN), String.valueOf(Role.SELLER))

                        .requestMatchers("/cart/").hasAuthority(String.valueOf(Role.USER))
                        .requestMatchers("/order/").hasAuthority(String.valueOf(Role.USER))
                        .requestMatchers("/custompc/build").hasAuthority(String.valueOf(Role.USER))
                        .requestMatchers("/seller/switch").hasAuthority(String.valueOf(Role.USER))
                        .requestMatchers("/seller/new").hasAuthority(String.valueOf(Role.USER))


                        .requestMatchers("/admin/add").hasAuthority(String.valueOf(Role.ADMIN))
                        .requestMatchers("/admin/productobserve").hasAuthority(String.valueOf(Role.ADMIN))
                        .requestMatchers("/admin/newproduct").hasAuthority(String.valueOf(Role.ADMIN))
                        .requestMatchers("/admin/addattributes").hasAuthority(String.valueOf(Role.ADMIN))
                        .requestMatchers("/admin/attributesobserve").hasAuthority(String.valueOf(Role.ADMIN))
                        .requestMatchers("/admin/newattributes").hasAuthority(String.valueOf(Role.ADMIN))
                        .requestMatchers("/admin/addvalues").hasAuthority(String.valueOf(Role.ADMIN))
                        .requestMatchers("/admin/valuesobserve").hasAuthority(String.valueOf(Role.ADMIN))
                        .requestMatchers("/admin/newattributevalues").hasAuthority(String.valueOf(Role.ADMIN))
                        .requestMatchers("/admin/default").hasAuthority(String.valueOf(Role.ADMIN))
                        .requestMatchers("/users").hasAuthority(String.valueOf(Role.ADMIN))

                        .requestMatchers("/seller/observe").hasAuthority(String.valueOf(Role.SELLER))
                        .requestMatchers("/seller/products").hasAuthority(String.valueOf(Role.SELLER))
                        .requestMatchers("/seller/add-to-store").hasAuthority(String.valueOf(Role.SELLER))
                        .requestMatchers("/seller/posttostore").hasAuthority(String.valueOf(Role.SELLER))
                        .requestMatchers("/seller/success").hasAuthority(String.valueOf(Role.SELLER))
                        .requestMatchers("/custompc/build").hasAuthority(String.valueOf(Role.SELLER))

                        .anyRequest().authenticated())
                .formLogin(form -> form
                        .loginPage("/login")
                        .successHandler(successHandler)
                        .permitAll()

                );
        return http.build();
    }


    @Autowired
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(PasswordEncoderConfig.passwordEncoder());
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(PasswordEncoderConfig.passwordEncoder());
        return authProvider;
    }
}
