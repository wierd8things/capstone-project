package com.epam.pcconfig.models.dto.pcconfigs;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
public class ConfigProductDto {
    private Long configId;
    private List<Long> productIds;
    private BigDecimal minPrice;
}
