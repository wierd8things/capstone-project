package com.epam.pcconfig.repositories;

import com.epam.pcconfig.models.ProductType;
import com.epam.pcconfig.models.dto.products.ProductTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductTypeRepository extends JpaRepository<ProductType, Long> {
    Optional<ProductType> findByProductTypeName(ProductTypeEnum typeName);

    ProductType findProductTypeByProductTypeName(ProductTypeEnum productType);
}
