package com.epam.pcconfig.models.dto.pcconfigs;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PCConfigDto {
    private Long userId;
    private String configName;
//    private Boolean isCustom;
    private LocalDate createDate;
}
