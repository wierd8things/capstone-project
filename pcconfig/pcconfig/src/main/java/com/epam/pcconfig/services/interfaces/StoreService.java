package com.epam.pcconfig.services.interfaces;

import com.epam.pcconfig.models.Store;

public interface StoreService {
    Store findStoreByUsername(String username);
}
