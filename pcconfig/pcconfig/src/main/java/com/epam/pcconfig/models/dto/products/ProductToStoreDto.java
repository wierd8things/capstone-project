package com.epam.pcconfig.models.dto.products;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ProductToStoreDto {
    private Long productId;
    private Long storeId;
    private BigDecimal price;
}
