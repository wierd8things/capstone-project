package com.epam.pcconfig.models;


import com.epam.pcconfig.models.embedded.ContactInfo;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "enabled")
    private boolean enabled;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride( name = "first_name", column = @Column(name = "first_name")),
            @AttributeOverride( name = "last_name", column = @Column(name = "last_name")),
            @AttributeOverride( name = "email", column = @Column(name = "email"))
    })
    private ContactInfo contactInfo;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Authority> authorities;

    @OneToOne(mappedBy = "user")
    private Store store;

    @OneToMany(mappedBy = "user")
    private List<PCConfiguration> pcConfigurations;

    @OneToOne(mappedBy = "user")
    private Cart cart;
 }
