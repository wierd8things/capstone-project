package com.epam.pcconfig.models.dto.products;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProductAttributeValueDto {
    private Long productId;
    private Long productAttributeId;
    private String value;
}
