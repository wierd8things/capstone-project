package com.epam.pcconfig.services.implementations;

import com.epam.pcconfig.models.ProductAttribute;
import com.epam.pcconfig.repositories.ProductAttributeRepository;
import com.epam.pcconfig.services.interfaces.ProductAttributeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductAttributeServiceImpl implements ProductAttributeService {
    @Autowired
    private final ProductAttributeRepository productAttributeRepository;

    public ProductAttributeServiceImpl(ProductAttributeRepository productAttributeRepository) {
        this.productAttributeRepository = productAttributeRepository;
    }

    @Override
    public List<ProductAttribute> getAllAttributes() {
        return productAttributeRepository.findAll();
    }
}
