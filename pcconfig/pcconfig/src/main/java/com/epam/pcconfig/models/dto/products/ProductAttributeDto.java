package com.epam.pcconfig.models.dto.products;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProductAttributeDto {
    private String attributeName;
    private MeasureUnitEnum measurementUnit;
}
