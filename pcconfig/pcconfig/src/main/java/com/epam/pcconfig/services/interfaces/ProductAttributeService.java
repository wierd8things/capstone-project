package com.epam.pcconfig.services.interfaces;

import com.epam.pcconfig.models.ProductAttribute;

import java.util.List;

public interface ProductAttributeService {
    List<ProductAttribute> getAllAttributes();

}
