package com.epam.pcconfig.mappers;

import com.epam.pcconfig.models.*;
import com.epam.pcconfig.models.dto.pcconfigs.ConfigProductDto;
import com.epam.pcconfig.models.dto.pcconfigs.PCConfigDto;
import com.epam.pcconfig.models.dto.products.ProductAttributeDto;
import com.epam.pcconfig.models.dto.products.ProductAttributeValueDto;
import com.epam.pcconfig.models.dto.products.ProductDto;
import com.epam.pcconfig.models.dto.products.ProductToStoreDto;
import com.epam.pcconfig.repositories.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Mappers {

    @Autowired
    private ModelMapper modelMapper;

    private ProductTypeRepository productTypeRepository;
    private ProductAttributeRepository productAttributeRepository;
    private ProductRepository productRepository;
    private StoreRepository storeRepository;
    private UserRepository userRepository;

    public ProductDto mapToProductDto(Product product) {
        return modelMapper.map(product, ProductDto.class);
    }

    public ProductAttributeDto mapToProductAttributeDto(ProductAttribute productAttribute) {
        return modelMapper.map(productAttribute, ProductAttributeDto.class);
    }

    public ProductAttributeValueDto mapToProductAttributeValueDto(ProductAttributeValue productAttributeValue) {
        return modelMapper.map(productAttributeValue, ProductAttributeValueDto.class);
    }

    public ProductToStoreDto mapToProductStore(ProductToStore productToStore) {
        ProductToStoreDto productToStoreDto = new ProductToStoreDto();

        productToStoreDto.setProductId(productToStore.getProduct().getProductId());
        productToStoreDto.setStoreId(productToStoreDto.getStoreId());
        productToStoreDto.setPrice(productToStore.getStorePrice());

        return productToStoreDto;
    }

    public ProductToStore toProductToStore(ProductToStoreDto productToStoreDto) {
        ProductToStore productToStore = new ProductToStore();

        Product product = productRepository.findById(productToStoreDto.getProductId())
                .orElseThrow(() -> new IllegalArgumentException("Product not found"));

        Store store = storeRepository.findById(productToStoreDto.getStoreId())
                .orElseThrow(() -> new IllegalArgumentException("Store not found"));

        productToStore.setProduct(product);
        productToStore.setStore(store);
        productToStore.setStorePrice(productToStoreDto.getPrice());

        return productToStore;
    }

    public PCConfigDto mapToPcConfigDto(PCConfiguration pcConfiguration) {
//        PCConfigDto pcConfigDto = new PCConfigDto();
//        pcConfigDto.setConfigName(pcConfiguration.getConfigName());
//        pcConfigDto.setUserId(pcConfiguration.getUser().getUserId());
//        pcConfigDto.setCustom(pcConfiguration.isCustom());
//        pcConfigDto.setCreateDate(pcConfiguration.getCreateDate());
//
//        return pcConfigDto;
        return modelMapper.map(pcConfiguration, PCConfigDto.class);
    }

    public PCConfiguration toPcConfiguration(PCConfigDto pcConfigDto) {
//        PCConfiguration pcConfiguration = new PCConfiguration();
//
//        User user = userRepository.findById(pcConfigDto.getUserId())
//                .orElseThrow(() -> new IllegalArgumentException("User not found"));
//
//        pcConfiguration.setUser(user);
//        pcConfiguration.setConfigName(pcConfigDto.getConfigName());
//        pcConfiguration.setCustom(pcConfigDto.isCustom());
//        pcConfiguration.setCreateDate(pcConfigDto.getCreateDate());
//
//        return pcConfiguration;
        return modelMapper.map(pcConfigDto, PCConfiguration.class);
    }

    public ConfigProductDto mapToConfigProductDto(PCConfigurationProduct pcConfigurationProduct) {
        return modelMapper.map(pcConfigurationProduct, ConfigProductDto.class);
    }

    public PCConfigurationProduct toPcConfigProduct(ConfigProductDto configProductDto) {
        return modelMapper.map(configProductDto, PCConfigurationProduct.class);
    }
}
