package com.epam.pcconfig.services.interfaces;

import com.epam.pcconfig.models.User;
import com.epam.pcconfig.models.dto.StoreDto;
import com.epam.pcconfig.models.dto.UserDto;
import com.epam.pcconfig.models.dto.UserRegisterDto;
import com.epam.pcconfig.roles.Role;

import java.util.List;

public interface UserService {
    User getUserByUsernameWithAuthorities(String username);

    User getUserByUsernameAndAuthority(String username, Role authority);

    boolean isSeller(User user);

    List<UserDto> getAllSellers();

    List<UserDto> getAllUsers();

    User findByUsername(String username);

    User findByUserId(Long userId);

    User banUser(Long userId);

    void registerUser(UserRegisterDto userRegistrationDto);

    User findByUserEmail(String email);

    void switchToSeller(StoreDto storeDto);
}
