package com.epam.pcconfig.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "products")
public class Product {

    public Product(Long productId) {
        this.productId = productId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private Long productId;

    @Column(name = "product_name", nullable = false)
    private String productName;

    @ManyToOne
    @JoinColumn(name = "product_type_id", referencedColumnName = "product_type_id", nullable = false)
    private ProductType productType;

    @OneToMany(mappedBy = "product")
    private List<ProductToStore> stores;

    @OneToMany(mappedBy = "product")
    private List<ProductAttributeValue> productAttributeValues;

    @OneToMany(mappedBy = "product")
    private List<PCConfigurationProduct> pcConfigurationProducts;
}
