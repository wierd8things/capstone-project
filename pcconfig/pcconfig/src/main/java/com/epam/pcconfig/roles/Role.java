package com.epam.pcconfig.roles;

public enum Role {
    ADMIN,
    USER,
    SELLER
}
