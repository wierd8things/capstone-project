package com.epam.pcconfig.repositories;

import com.epam.pcconfig.models.PCConfiguration;
import com.epam.pcconfig.models.Product;
import com.epam.pcconfig.models.dto.products.ProductTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query("SELECT p FROM Product p WHERE p.productType.productTypeId = :productTypeId")
    List<Product> findByProductTypeId(Long productType);

    List<Product> findProductByProductType_ProductTypeName(ProductTypeEnum productType);

    @Query("SELECT p FROM Product p " +
            "JOIN ProductToStore pts ON p.productId = pts.product.productId " +
            "WHERE p.productType.productTypeId = :productTypeId " +
            "ORDER BY pts.storePrice")
    List<Product> findByProductTypeIdOrderByPrice(Long productTypeId);

    @Query("SELECT p FROM Product p JOIN p.pcConfigurationProducts pc WHERE pc.pcConfiguration = :pcConfiguration")
    List<Product> findProductsByPCConfiguration(@Param("pcConfiguration") PCConfiguration pcConfiguration);

    @Query("SELECT p FROM Product p " +
            "JOIN ProductToStore pts ON p.productId = pts.product.productId " +
            "JOIN Store s ON pts.store.storeId = s.storeId " +
            "JOIN User u ON s.user.userId = u.userId " +
            "WHERE u.username = :username")
    List<Product> findByUsername(@Param("username") String username);

    Product findProductByProductName(String productName);
}
