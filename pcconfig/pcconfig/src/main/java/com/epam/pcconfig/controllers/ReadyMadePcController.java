package com.epam.pcconfig.controllers;

import com.epam.pcconfig.models.PCConfiguration;
import com.epam.pcconfig.roles.Role;
import com.epam.pcconfig.services.implementations.PCConfigurationServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/readymadepcs")
public class ReadyMadePcController {

    private final PCConfigurationServiceImpl pcConfiguration;

    public ReadyMadePcController(PCConfigurationServiceImpl pcConfiguration) {
        this.pcConfiguration = pcConfiguration;
    }

    @GetMapping("/observe")
    public String readyMadeConfigs(Model model) {
        List<PCConfiguration> readyConfigs = pcConfiguration.getPCConfigsWhereUserIsSeller(Role.SELLER);
        model.addAttribute("readymadepcs", readyConfigs);

        return "/readymadepc/readymadepcs";
    }
}
