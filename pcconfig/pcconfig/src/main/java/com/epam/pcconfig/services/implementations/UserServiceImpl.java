package com.epam.pcconfig.services.implementations;

import com.epam.pcconfig.models.Authority;
import com.epam.pcconfig.models.Store;
import com.epam.pcconfig.models.User;
import com.epam.pcconfig.models.dto.StoreDto;
import com.epam.pcconfig.models.dto.UserDto;
import com.epam.pcconfig.models.dto.UserRegisterDto;
import com.epam.pcconfig.models.embedded.ContactInfo;
import com.epam.pcconfig.repositories.AuthorityRepository;
import com.epam.pcconfig.repositories.StoreRepository;
import com.epam.pcconfig.repositories.UserRepository;
import com.epam.pcconfig.roles.Role;
import com.epam.pcconfig.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    private final StoreRepository storeRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository, StoreRepository storeRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
        this.storeRepository = storeRepository;
    }

    @Override
    public User getUserByUsernameWithAuthorities(String username) {
        return userRepository.findByUsernameWithAuthorities(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    @Override
    public User getUserByUsernameAndAuthority(String username, Role authority) {
        return userRepository.findUserByUsernameAndAuthorities_Authority(username, authority);
    }

    @Override
    public boolean isSeller(User user) {
        return user.getAuthorities().stream().anyMatch(role -> role.getAuthority().equals(Role.SELLER));
    }

    @Override
    public List<UserDto> getAllSellers() {
        if (authorityRepository.existsByAuthority(Role.SELLER)) {
            List<User> sellers = userRepository.findByAuthorities_Authority(Role.SELLER);
            return sellers.stream()
                    .map(this::mapToUserDto)
                    .collect(Collectors.toList());
        } else {
            throw new RuntimeException();
        }
    }



    @Override
    public List<UserDto> getAllUsers() {
        if (authorityRepository.existsByAuthority(Role.USER)) {
            List<User> users = userRepository.findByAuthorities_Authority(Role.USER);
            return users.stream()
                    .map(this::mapToUserDto)
                    .collect(Collectors.toList());
        } else {
            throw new RuntimeException();
        }
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User findByUserId(Long userId) {
        return userRepository.findByUserId(userId)
                .orElseThrow(() -> new IllegalArgumentException("User not found"));
    }

    private UserDto mapToUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setUserId(user.getUserId());
        userDto.setFirstName(user.getContactInfo().getFirst_name());
        userDto.setLastName(user.getContactInfo().getLast_name());
        userDto.setEmail(user.getContactInfo().getEmail());
        userDto.setUsername(user.getUsername());
        userDto.setEnabled(user.isEnabled());

        List<String> roles = user.getAuthorities().stream()
                .map(authority -> authority.getAuthority().name())
                .collect(Collectors.toList());
        userDto.setRoles(roles);

        return userDto;
    }

    @Override
    public User banUser(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new RuntimeException("User not found"));
        user.setEnabled(false);
        userRepository.save(user);

        return user;
    }

    @Override
    public void registerUser(UserRegisterDto userRegistrationDto) {
        User user = new User();
        user.setUsername(userRegistrationDto.getUsername());
        user.setPassword(passwordEncoder.encode(userRegistrationDto.getPassword()));

        ContactInfo contactInfo = new ContactInfo();
        contactInfo.setFirst_name(userRegistrationDto.getFirstName());
        contactInfo.setLast_name(userRegistrationDto.getLastName());
        contactInfo.setEmail(userRegistrationDto.getEmail());
        user.setContactInfo(contactInfo);

        user.setEnabled(true);
        userRepository.save(user);

        // Создание нового объекта Authority и добавление его в список пользователя
        List<Authority> authorities = new ArrayList<>();
        Authority authority = new Authority();
        authority.setAuthority(Role.USER);
        authority.setUser(user);
        authorities.add(authority);
        user.setAuthorities(authorities);

        userRepository.save(user);
        authorityRepository.save(authority);
    }

    @Override
    public User findByUserEmail(String email) {
        return userRepository.findByContactInfo_Email(email);
    }


    @Override
    public void switchToSeller(StoreDto storeDto) {
        User user = userRepository.findByUsername(storeDto.getUsername());

        if (user != null) {
            List<Authority> authorities = user.getAuthorities();
            Authority sellerAuthority = new Authority();
            sellerAuthority.setUser(user);
            sellerAuthority.setAuthority(Role.SELLER);
            authorities.add(sellerAuthority);
            authorityRepository.save(sellerAuthority);
            user.setAuthorities(authorities);
            userRepository.save(user);

            Store store = new Store();
            store.setUser(user);
            store.setStoreName(storeDto.getStoreName());
            store.setAddress(storeDto.getAddress());
            store.setContactEmail(storeDto.getContactEmail());
            store.setStatus(storeDto.getStatus());
            storeRepository.save(store);
        }
    }

    public StoreDto parseToStoreDTO(String username, String storeName, String address, String contactEmail, String status) {
        StoreDto storeDTO = new StoreDto();
        storeDTO.setUsername(username);
        storeDTO.setStoreName(storeName);
        storeDTO.setAddress(address);
        storeDTO.setContactEmail(contactEmail);
        storeDTO.setStatus(status);
        return storeDTO;
    }
}
