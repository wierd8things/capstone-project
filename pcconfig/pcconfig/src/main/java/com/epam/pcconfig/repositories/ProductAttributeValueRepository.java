package com.epam.pcconfig.repositories;

import com.epam.pcconfig.models.ProductAttributeValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductAttributeValueRepository extends JpaRepository<ProductAttributeValue, Long> {
    List<ProductAttributeValue> findProductAttributeValueByProduct_ProductId(Long productId);
}
