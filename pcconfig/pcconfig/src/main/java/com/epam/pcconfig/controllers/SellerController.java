package com.epam.pcconfig.controllers;

import com.epam.pcconfig.models.Product;
import com.epam.pcconfig.models.User;
import com.epam.pcconfig.models.dto.StoreDto;
import com.epam.pcconfig.models.dto.products.ProductToStoreDto;
import com.epam.pcconfig.roles.Role;
import com.epam.pcconfig.services.implementations.CustomUserDetailsService;
import com.epam.pcconfig.services.implementations.ProductServiceImpl;
import com.epam.pcconfig.services.implementations.StoreServiceImpl;
import com.epam.pcconfig.services.implementations.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/seller")
public class SellerController {

    private final UserServiceImpl userService;

    private final ProductServiceImpl productService;

    private final StoreServiceImpl storeService;

    private final CustomUserDetailsService customUserDetailsService;

    @Autowired
    public SellerController(UserServiceImpl userService, ProductServiceImpl productService, StoreServiceImpl storeService, CustomUserDetailsService customUserDetailsService) {
        this.userService = userService;
        this.productService = productService;
        this.storeService = storeService;
        this.customUserDetailsService = customUserDetailsService;
    }

    @GetMapping("/success")
    public String sellerSuccess() {
        return "seller/sellersuccess";
    }

    @GetMapping("/observe")
    public String sellerPage(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        List<Product> products = productService.getAllProductsBySeller(username);
        model.addAttribute("products", products);
        model.addAttribute("store", storeService.findStoreByUsername(username));
        model.addAttribute("seller", userService.getUserByUsernameAndAuthority(username, Role.SELLER));

        return "/seller/sellerpage";
    }

    @GetMapping("/new")
    public String showSwitchToSellerForm(Model model) {
        StoreDto storeDto = new StoreDto();
        model.addAttribute("seller", storeDto);

        return "/seller/sellerform";
    }

    @PostMapping("/switch")
    public String switchToSeller(@ModelAttribute("seller") StoreDto storeDto,
                                 Model model) {
        // Получаем текущего пользователя по имени пользователя
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        User user = userService.findByUsername(username);

        if (userService.isSeller(user)) {
            model.addAttribute("errorMessage", "The user is already seller");
            return "/seller/sellerform";
        }

        model.addAttribute("seller", storeDto);

        userService.switchToSeller(storeDto);

        // Refresh the authentication
        UserDetails userDetails = customUserDetailsService.loadUserByUsername(username);
        Authentication newAuth = new UsernamePasswordAuthenticationToken(userDetails, authentication.getCredentials(), userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(newAuth);

        return "redirect:/seller/success"; // Редирект на страницу продавца
    }

    @GetMapping("/products")
    public String getSellerProducts(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String seller = authentication.getName();

        List<Product> products = productService.getAllProductsBySeller(seller);
        model.addAttribute("products", products);

        return "/seller/sellerproduct";
    }


    // Works fine
    @GetMapping("/add-to-store")
    public String addToStoreForm(Model model) {
        List<Product> products = productService.findAllProducts();

        model.addAttribute("products", products);
        model.addAttribute("productToStoreDto", new ProductToStoreDto());

        return "/seller/addproducttostoreform";
    }


    // Works fine
    @PostMapping("/posttostore")
    public String addToStore(@ModelAttribute("productToStoreDto") ProductToStoreDto productToStoreDto) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String sellerId = authentication.getName();

        productService.addProductToStore(productToStoreDto, sellerId);

        return "redirect:/seller/observe";
    }
}
