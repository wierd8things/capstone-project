package com.epam.pcconfig.services.interfaces;

import com.epam.pcconfig.models.Product;
import com.epam.pcconfig.models.ProductAttribute;
import com.epam.pcconfig.models.ProductAttributeValue;
import com.epam.pcconfig.models.dto.products.*;

import java.util.List;

public interface ProductService {

    List<Product> getAllProductsByTypeName(ProductTypeEnum productType);

    List<Product> getProductsByTypeAndSortedByPrice(Long productType);

    List<Product> getAllProducts();

    List<Product> getAllProductsBySeller(String seller);

    Product addProduct(ProductDto productDto);

    ProductAttribute addProductAttribute(ProductAttributeDto productAttributeDto);

    ProductAttributeValue addProductAttributeValue(ProductAttributeValueDto productAttributeValueDto);

    void addProductToStore(ProductToStoreDto productToStoreDto, String seller);

    List<Product> findAllProducts();

    List<ProductAttribute> findAllAttributes();

    Product getProductById(Long productId);

    List<ProductAttributeValue> getAllProductValuesByProductId(Long productId);

    List<ProductAttributeValue> getAllProductValues();
}
