package com.epam.pcconfig.services.implementations;

import com.epam.pcconfig.models.PCConfigurationProduct;
import com.epam.pcconfig.models.ProductType;
import com.epam.pcconfig.repositories.PCConfigurationProductRepository;
import com.epam.pcconfig.services.interfaces.PCConfigurationProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PCConfigurationProductServiceImpl implements PCConfigurationProductService {
    private final PCConfigurationProductRepository pcConfigurationProductRepository;

    @Autowired
    public PCConfigurationProductServiceImpl(PCConfigurationProductRepository pcConfigurationProductRepository) {
        this.pcConfigurationProductRepository = pcConfigurationProductRepository;
    }

    @Override
    public List<PCConfigurationProduct> getAllProductsByConfigId(Long configId) {
        return pcConfigurationProductRepository.findPCConfigurationProductsByPcConfiguration_ConfigId(configId);
    }

    @Override
    public List<PCConfigurationProduct> getAllProductsByConfigIdAndProductType(ProductType productType, Long configId) {
        return pcConfigurationProductRepository.findProductByProduct_ProductTypeAndPcConfiguration_ConfigId(productType, configId);
    }

}
