package com.epam.pcconfig.models;

import com.epam.pcconfig.models.dto.orders.StatusEnum;
import com.epam.pcconfig.models.embedded.FullAddress;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private Long orderId;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    private User user;

    @OneToOne
    @JoinColumn(name = "cart_id", referencedColumnName = "cart_id", nullable = false)
    private Cart cart;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusEnum status;

    @Column(name = "total_price")
    private BigDecimal totalPrice;

    @Column(name = "place_order_date")
    private LocalDate placeOrderDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "shipping_type")
    private ShippingTypeEnum shippingType;

    @Column(name = "postal_code")
    private Integer postelCode;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride( name = "country", column = @Column(name = "country")),
            @AttributeOverride( name = "city", column = @Column(name = "city")),
            @AttributeOverride( name = "address", column = @Column(name = "address"))
    })
    private FullAddress fullAddress;
}
