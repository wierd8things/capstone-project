package com.epam.pcconfig.services.implementations;

import com.epam.pcconfig.models.ProductToStore;
import com.epam.pcconfig.models.ProductType;
import com.epam.pcconfig.repositories.ProductToStoreRepository;
import com.epam.pcconfig.services.interfaces.ProductToStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductToStoreServiceImpl implements ProductToStoreService {
    @Autowired
    private final ProductToStoreRepository productToStoreRepository;

    public ProductToStoreServiceImpl(ProductToStoreRepository productToStoreRepository) {
        this.productToStoreRepository = productToStoreRepository;
    }

    @Override
    public List<ProductToStore> getProductsByTypeOrderByPrice(ProductType productType) {
        return productToStoreRepository.findByProduct_ProductType(productType);
    }
}
