package com.epam.pcconfig.services.implementations;

import com.epam.pcconfig.models.Cart;
import com.epam.pcconfig.models.PCConfiguration;
import com.epam.pcconfig.models.PCConfigurationProduct;
import com.epam.pcconfig.models.User;
import com.epam.pcconfig.repositories.CartRepository;
import com.epam.pcconfig.repositories.PCConfigurationProductRepository;
import com.epam.pcconfig.repositories.PCConfigurationRepository;
import com.epam.pcconfig.repositories.UserRepository;
import com.epam.pcconfig.services.interfaces.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

@Service
public class CartServiceImpl implements CartService {
    private final CartRepository cartRepository;
    private final UserRepository userRepository;
    private final PCConfigurationRepository pcConfigurationRepository;
    private final PCConfigurationProductRepository pcConfigurationProductRepository;

    @Autowired
    public CartServiceImpl(CartRepository cartRepository, UserRepository userRepository, PCConfigurationRepository pcConfigurationRepository, PCConfigurationProductRepository pcConfigurationProductRepository) {
        this.cartRepository = cartRepository;
        this.userRepository = userRepository;
        this.pcConfigurationRepository = pcConfigurationRepository;
        this.pcConfigurationProductRepository = pcConfigurationProductRepository;
    }

    @Override
    public void addPCConfigToCart(String username) {
        User user = userRepository.findByUsername(username);

        PCConfiguration pcConfiguration = pcConfigurationRepository.findLatestByUsername(user.getUsername());

        BigDecimal totalPrice = BigDecimal.ZERO;

        List<PCConfigurationProduct> products = pcConfigurationProductRepository.findPCConfigurationProductsByPcConfiguration_ConfigId(pcConfiguration.getConfigId());
        for (PCConfigurationProduct product : products) {
            totalPrice = totalPrice.add(product.getMinPrice());
        }

        Cart cart = new Cart();
        cart.setUser(user);
        cart.setPcConfiguration(pcConfiguration);
        cart.setAddToCartDate(LocalDate.now(ZoneId.systemDefault()));
        cart.setTotalPrice(totalPrice);

        cartRepository.save(cart);
    }

    @Override
    public Cart showUserCart(String username) {
        return cartRepository.findByUserUsername(username);
    }
}
