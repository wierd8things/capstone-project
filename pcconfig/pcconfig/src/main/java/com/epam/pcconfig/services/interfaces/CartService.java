package com.epam.pcconfig.services.interfaces;

import com.epam.pcconfig.models.Cart;

public interface CartService {
    void addPCConfigToCart(String username);

    Cart showUserCart(String username);
}
