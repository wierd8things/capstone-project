package com.epam.pcconfig.models.dto.orders;

import com.epam.pcconfig.models.ShippingTypeEnum;
import com.epam.pcconfig.models.embedded.FullAddress;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class OrderDto {
    private Long userId;
    private Long cartId;
    private StatusEnum status;
    private BigDecimal totalPrice;
    private LocalDate placeOrderDate;
    private ShippingTypeEnum shippingType;
    private FullAddress fullAddress;
    private int postelCode;
}
