package com.epam.pcconfig.repositories;

import com.epam.pcconfig.models.Store;
import com.epam.pcconfig.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StoreRepository extends JpaRepository<Store, Long> {
    Store findStoreByUserUsername(String username);

    Optional<Store> findStoreByUser_UserId(Long userId);

    Store findByUser(User user);
}
