package com.epam.pcconfig.repositories;

import com.epam.pcconfig.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    Optional<Order> findByUserUserId(Long userId);

    Order findByUserUsername(String username);
}
