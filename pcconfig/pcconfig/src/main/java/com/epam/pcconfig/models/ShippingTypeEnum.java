package com.epam.pcconfig.models;

public enum ShippingTypeEnum {
    COURIER,
    SELF_DELIVERY,
    MAIL
}
