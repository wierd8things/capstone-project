package com.epam.pcconfig.models.dto.pcconfigs;

import com.epam.pcconfig.models.dto.products.ProductDto;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class FullConfigInfo {
    private Long configId;
    private String configName;
    private BigDecimal totalPrice;
    private boolean isCustom;
    private LocalDate createDate;
    private Long userId;
    private List<ProductDto> products;
}
