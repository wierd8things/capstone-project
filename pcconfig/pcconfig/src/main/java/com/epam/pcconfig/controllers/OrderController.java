package com.epam.pcconfig.controllers;

import com.epam.pcconfig.models.Order;
import com.epam.pcconfig.models.dto.orders.OrderDto;
import com.epam.pcconfig.services.implementations.OrderServiceImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/orders")
public class OrderController {

    private final OrderServiceImpl orderService;

    public OrderController(OrderServiceImpl orderService) {
        this.orderService = orderService;
    }

    @GetMapping()
    public String getOrders(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        Order order = orderService.getOrdersByUserId(username);
        model.addAttribute("order", order);

        return "/checkout/order";
    }

    @GetMapping("/checkout")
    public String checkoutPage(Model model) {
        model.addAttribute("order", new OrderDto());

        return "/checkout/checkout";
    }

    @PostMapping("/place-order")
    public String placeOrder(@ModelAttribute("order") OrderDto orderDto,
                             RedirectAttributes redirectAttributes) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        orderService.addCartToOrder(username, orderDto);
        redirectAttributes.addFlashAttribute("saveMessage", "Order placed successfully");

        return "redirect:/orders";
    }
}
