package com.epam.pcconfig.services.implementations;

import com.epam.pcconfig.models.Cart;
import com.epam.pcconfig.models.Order;
import com.epam.pcconfig.models.User;
import com.epam.pcconfig.models.dto.orders.OrderDto;
import com.epam.pcconfig.models.dto.orders.StatusEnum;
import com.epam.pcconfig.repositories.CartRepository;
import com.epam.pcconfig.repositories.OrderRepository;
import com.epam.pcconfig.repositories.UserRepository;
import com.epam.pcconfig.services.interfaces.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;

@Service
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final CartRepository cartRepository;
    private final UserRepository userRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, CartRepository cartRepository, UserRepository userRepository) {
        this.orderRepository = orderRepository;
        this.cartRepository = cartRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Order addCartToOrder(String username, OrderDto orderDto) {
        Order userOrder = new Order();

        User user = userRepository.findByUsername(username);
        Cart cart = cartRepository.findByUserUsername(username);

        userOrder.setCart(cart);
        userOrder.setUser(user);
        userOrder.setPlaceOrderDate(LocalDate.now(ZoneId.systemDefault()));
        userOrder.setStatus(StatusEnum.ON_THE_WAY);
        userOrder.setFullAddress(orderDto.getFullAddress());
        userOrder.setShippingType(orderDto.getShippingType());
        userOrder.setPostelCode(orderDto.getPostelCode());
        userOrder.setTotalPrice(cart.getTotalPrice());
        return orderRepository.save(userOrder);
    }

    @Override
    public Order getOrdersByUserId(String username) {
        return orderRepository.findByUserUsername(username);
    }
}
