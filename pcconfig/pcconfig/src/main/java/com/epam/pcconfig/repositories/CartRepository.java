package com.epam.pcconfig.repositories;

import com.epam.pcconfig.models.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {
    Optional<Cart> findByUser_UsernameAndPcConfigurationConfigId(String username, Long configId);

    Cart findByUserUsername(String username);

    Cart findByUserUserId(Long userId);
}
