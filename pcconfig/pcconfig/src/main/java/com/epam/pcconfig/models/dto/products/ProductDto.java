package com.epam.pcconfig.models.dto.products;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProductDto {
    private String name;
    private ProductTypeEnum productType;
}
