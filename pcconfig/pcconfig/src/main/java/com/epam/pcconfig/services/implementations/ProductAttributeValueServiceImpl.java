package com.epam.pcconfig.services.implementations;

import com.epam.pcconfig.models.ProductAttributeValue;
import com.epam.pcconfig.repositories.ProductAttributeValueRepository;
import com.epam.pcconfig.services.interfaces.ProductAttributeValueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductAttributeValueServiceImpl implements ProductAttributeValueService {
    @Autowired
    private final ProductAttributeValueRepository productAttributeValueRepository;

    public ProductAttributeValueServiceImpl(ProductAttributeValueRepository productAttributeValueRepository) {
        this.productAttributeValueRepository = productAttributeValueRepository;
    }

    @Override
    public List<ProductAttributeValue> getAllProductAttributeValues() {
        return productAttributeValueRepository.findAll();
    }
}
