package com.epam.pcconfig.services.interfaces;

import com.epam.pcconfig.models.ProductAttributeValue;

import java.util.List;

public interface ProductAttributeValueService {
    List<ProductAttributeValue> getAllProductAttributeValues();
}
