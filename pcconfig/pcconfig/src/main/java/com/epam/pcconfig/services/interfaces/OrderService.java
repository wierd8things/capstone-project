package com.epam.pcconfig.services.interfaces;

import com.epam.pcconfig.models.Order;
import com.epam.pcconfig.models.dto.orders.OrderDto;

public interface OrderService {
    Order addCartToOrder(String username, OrderDto orderDto);

    Order getOrdersByUserId(String username);
}
