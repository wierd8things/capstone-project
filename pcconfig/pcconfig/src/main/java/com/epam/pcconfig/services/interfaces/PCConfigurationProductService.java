package com.epam.pcconfig.services.interfaces;

import com.epam.pcconfig.models.PCConfigurationProduct;
import com.epam.pcconfig.models.ProductType;

import java.util.List;

public interface PCConfigurationProductService {
    List<PCConfigurationProduct> getAllProductsByConfigId(Long configId);

    List<PCConfigurationProduct> getAllProductsByConfigIdAndProductType(ProductType productType, Long configId);
}
