package com.epam.pcconfig.controllers;

import com.epam.pcconfig.models.Cart;
import com.epam.pcconfig.services.implementations.CartServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/userCart")
public class CartController {
    private final CartServiceImpl cartService;

    @Autowired
    public CartController(CartServiceImpl cartService) {
        this.cartService = cartService;
    }

    @GetMapping("/observe")
    public String showCart(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        Cart cart = cartService.showUserCart(username);
        model.addAttribute("cart", cart);

        return "/cart/cart";
    }

    @PostMapping("/add")
    public String addToCart(RedirectAttributes redirectAttributes) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        cartService.addPCConfigToCart(username);
        redirectAttributes.addFlashAttribute("saveMessage", "Config has been added to cart successfully");

        return "redirect:/userCart/observe";
    }
}
