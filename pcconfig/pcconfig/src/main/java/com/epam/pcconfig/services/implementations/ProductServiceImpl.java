package com.epam.pcconfig.services.implementations;

import com.epam.pcconfig.models.*;
import com.epam.pcconfig.models.dto.products.*;
import com.epam.pcconfig.repositories.*;
import com.epam.pcconfig.services.interfaces.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    private final ProductAttributeRepository productAttributeRepository;

    private final StoreRepository storeRepository;

    private final ProductToStoreRepository productToStoreRepository;

    private final ProductAttributeValueRepository productAttributeValueRepository;

    private final ProductTypeRepository productTypeRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, ProductAttributeRepository productAttributeRepository, StoreRepository storeRepository, ProductToStoreRepository productToStoreRepository, ProductAttributeValueRepository productAttributeValueRepository, ProductTypeRepository productTypeRepository) {
        this.productRepository = productRepository;
        this.productAttributeRepository = productAttributeRepository;
        this.storeRepository = storeRepository;
        this.productToStoreRepository = productToStoreRepository;
        this.productAttributeValueRepository = productAttributeValueRepository;
        this.productTypeRepository = productTypeRepository;
    }

    @Override
    public List<Product> getAllProductsByTypeName(ProductTypeEnum productType) {
        return productRepository.findProductByProductType_ProductTypeName(productType);
    }

    @Override
    public List<Product> getProductsByTypeAndSortedByPrice(Long productTypeId) {
        return productRepository.findByProductTypeIdOrderByPrice(productTypeId);
    }



    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public List<Product> getAllProductsBySeller(String seller) {
        Store store = storeRepository.findStoreByUserUsername(seller);

        return productToStoreRepository.findByStoreId(store.getStoreId());
    }

    @Override
    public Product addProduct(ProductDto productDto) {
        Product product = new Product();
        product.setProductName(productDto.getName());

        ProductType productType = productTypeRepository.findByProductTypeName(productDto.getProductType())
                .orElseThrow(() -> new IllegalArgumentException("Invalid product type"));

        product.setProductType(productType);

        return productRepository.save(product);
    }

    @Override
    public ProductAttribute addProductAttribute(ProductAttributeDto productAttributeDto) {
        ProductAttribute productAttribute = new ProductAttribute();

        productAttribute.setAttributeName(productAttributeDto.getAttributeName());
        productAttribute.setMeasurementUnit(productAttributeDto.getMeasurementUnit());

        return productAttributeRepository.save(productAttribute);
    }

    @Override
    public ProductAttributeValue addProductAttributeValue(ProductAttributeValueDto productAttributeValueDto) {
        ProductAttributeValue productAttributeValue = new ProductAttributeValue();

        Product product = productRepository.findById(productAttributeValueDto.getProductId())
                .orElseThrow(() -> new IllegalArgumentException("Invalid product ID"));

        ProductAttribute productAttribute = productAttributeRepository.findById(productAttributeValueDto.getProductAttributeId())
                .orElseThrow(() -> new IllegalArgumentException("Invalid attribute ID"));

        productAttributeValue.setProduct(product);
        productAttributeValue.setProductAttribute(productAttribute);
        productAttributeValue.setValue(productAttributeValueDto.getValue());

        return productAttributeValueRepository.save(productAttributeValue);
    }

    @Override
    public void addProductToStore(ProductToStoreDto productToStoreDto, String seller) {
        Product product = productRepository.findById(productToStoreDto.getProductId())
                .orElseThrow(() -> new IllegalArgumentException("Invalid product ID"));

        Store store = storeRepository.findStoreByUserUsername(seller);

        ProductToStore productToStore = new ProductToStore();
        productToStore.setStore(store);
        productToStore.setProduct(product);
        productToStore.setStorePrice(productToStoreDto.getPrice());

        productToStoreRepository.save(productToStore);
    }

    @Override
    public List<Product> findAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public List<ProductAttribute> findAllAttributes() {
        return productAttributeRepository.findAll();
    }

    @Override
    public Product getProductById(Long productId) {
        return productRepository.findById(productId)
                .orElseThrow(() -> new IllegalArgumentException("Product not found"));
    }

    @Override
    public List<ProductAttributeValue> getAllProductValuesByProductId(Long productId) {
        return productAttributeValueRepository.findProductAttributeValueByProduct_ProductId(productId);
    }

    @Override
    public List<ProductAttributeValue> getAllProductValues() {
        return productAttributeValueRepository.findAll();
    }
}
