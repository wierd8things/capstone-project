package com.epam.pcconfig.controllers;

import com.epam.pcconfig.models.PCConfiguration;
import com.epam.pcconfig.models.Product;
import com.epam.pcconfig.models.ProductAttribute;
import com.epam.pcconfig.models.ProductAttributeValue;
import com.epam.pcconfig.models.dto.products.ProductAttributeDto;
import com.epam.pcconfig.models.dto.products.ProductAttributeValueDto;
import com.epam.pcconfig.models.dto.products.ProductDto;
import com.epam.pcconfig.services.implementations.PCConfigurationServiceImpl;
import com.epam.pcconfig.services.implementations.ProductAttributeServiceImpl;
import com.epam.pcconfig.services.implementations.ProductServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private final ProductServiceImpl productService;

    private final ProductAttributeServiceImpl productAttributeService;

    private final PCConfigurationServiceImpl pcConfigurationService;

    public AdminController(ProductServiceImpl productService, ProductAttributeServiceImpl productAttributeService, PCConfigurationServiceImpl pcConfigurationService) {
        this.productService = productService;
        this.productAttributeService = productAttributeService;
        this.pcConfigurationService = pcConfigurationService;
    }

    @GetMapping()
    public String adminDefault() {
        return "/admin/default";
    }

    @GetMapping("/allconfigs")
    public String showAllConfigs(Model model) {
        List<PCConfiguration> configs = pcConfigurationService.getAllPCConfigs();
        model.addAttribute("configs", configs);

        return "/admin/allconfigs";
    }

    @GetMapping("/add")
    public String addProductForm(Model model) {
        model.addAttribute("productDto", new ProductDto());

        return "/admin/addproductform";
    }

    @GetMapping("/productobserve")
    public String productObserve(Model model) {
        List<Product> products = productService.findAllProducts();

        model.addAttribute("products", products);

        return "/admin/productobserve";
    }

    @PostMapping("/newproduct")
    public String addProduct(@ModelAttribute ProductDto productDto,
                             Model model) {
        model.addAttribute("product", productService.addProduct(productDto));

        return "redirect:/admin/addattributes";
    }

    @GetMapping("/addattributes")
    public String addAttributeForm(Model model) {
        model.addAttribute("productAttributeDto", new ProductAttributeDto());

        return "/admin/addattributesform";
    }

    @GetMapping("/attributesobserve")
    public String attributesObserve(Model model) {
        List<ProductAttribute> attributes = productAttributeService.getAllAttributes();
        model.addAttribute("attributes", attributes);

        return "/admin/attributesobserve";
    }

    @PostMapping("/newattributes")
    public String addProductAttributes(@ModelAttribute ProductAttributeDto productAttributeDto,
                                       Model model) {
        model.addAttribute("productAttribute", productService.addProductAttribute(productAttributeDto));

        return "redirect:/admin/attributesobserve";
    }

    @GetMapping("/addvalues")
    public String addProductValuesForm(Model model) {
        model.addAttribute("attributes", productAttributeService.getAllAttributes());
        model.addAttribute("products", productService.findAllProducts());
        model.addAttribute("productValues", new ProductAttributeValueDto());

        return "/admin/addvaluesform";
    }

    @GetMapping("/valuesobserve")
    public String valuesObserve(Model model) {
        List<ProductAttributeValue> productAttributeValues = productService.getAllProductValues();

        model.addAttribute("productValues", productAttributeValues);

        return "/admin/valuesobserve";
    }

    @PostMapping("/newattributevalues")
    public String addProductValues(@ModelAttribute ProductAttributeValueDto productAttributeValueDto,
                                   Model model) {
        model.addAttribute("productValues", productService.addProductAttributeValue(productAttributeValueDto));

        return "redirect:/admin/valuesobserve";
    }
}
