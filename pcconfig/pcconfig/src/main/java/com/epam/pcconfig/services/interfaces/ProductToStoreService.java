package com.epam.pcconfig.services.interfaces;

import com.epam.pcconfig.models.ProductToStore;
import com.epam.pcconfig.models.ProductType;

import java.util.List;

public interface ProductToStoreService {
    List<ProductToStore> getProductsByTypeOrderByPrice(ProductType productType);
}
