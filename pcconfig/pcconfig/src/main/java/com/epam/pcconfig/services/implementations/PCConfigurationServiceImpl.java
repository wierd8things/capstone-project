package com.epam.pcconfig.services.implementations;

import com.epam.pcconfig.models.PCConfiguration;
import com.epam.pcconfig.models.PCConfigurationProduct;
import com.epam.pcconfig.models.Product;
import com.epam.pcconfig.models.User;
import com.epam.pcconfig.models.dto.pcconfigs.PCConfigDto;
import com.epam.pcconfig.repositories.*;
import com.epam.pcconfig.roles.Role;
import com.epam.pcconfig.services.interfaces.PCConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

@Service
public class PCConfigurationServiceImpl implements PCConfigurationService {
    private final PCConfigurationRepository pcConfigurationRepository;
    private final UserRepository userRepository;
    private final ProductToStoreRepository productToStoreRepository;
    private final PCConfigurationProductRepository pcConfigurationProductRepository;
    private final ProductRepository productRepository;

    @Autowired
    public PCConfigurationServiceImpl(PCConfigurationRepository pcConfigurationRepository, UserRepository userRepository, ProductToStoreRepository productToStoreRepository, PCConfigurationProductRepository pcConfigurationProductRepository, ProductRepository productRepository) {
        this.pcConfigurationRepository = pcConfigurationRepository;
        this.userRepository = userRepository;
        this.productToStoreRepository = productToStoreRepository;
        this.pcConfigurationProductRepository = pcConfigurationProductRepository;
        this.productRepository = productRepository;
    }

    @Override
    public List<PCConfiguration> getPCConfigsWhereUserIsSeller(Role role) {
        return pcConfigurationRepository.findAllByUser_Authorities_Authority(Role.SELLER);
    }

    @Override
    public PCConfiguration postNewCustomPC(PCConfigDto pcConfigDto, String username) {
        PCConfiguration pcConfiguration = new PCConfiguration();
        User user = userRepository.findByUsername(username);

        pcConfiguration.setUser(user);
        pcConfiguration.setConfigName(pcConfigDto.getConfigName());
        pcConfiguration.setCreateDate(LocalDate.now(ZoneId.systemDefault()));

        boolean hasSellerRole = user.getAuthorities().stream()
                .anyMatch(authority -> authority.getAuthority().equals(Role.SELLER));


        pcConfiguration.setIsCustom(!hasSellerRole);

        pcConfigurationRepository.save(pcConfiguration);

        return pcConfiguration;
    }

    @Override
    public PCConfigurationProduct assignProductToPc(String username, Long productId) {
        PCConfigurationProduct pcConfigurationProduct = new PCConfigurationProduct();

        PCConfiguration pcConfiguration = pcConfigurationRepository.findLatestByUsername(username);

        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new IllegalArgumentException("Product not found"));

        BigDecimal minPrice = productToStoreRepository.findMinPriceByProductId(product.getProductId());

        pcConfigurationProduct.setPcConfiguration(pcConfiguration);
        pcConfigurationProduct.setProduct(product);
        pcConfigurationProduct.setMinPrice(minPrice);

        return pcConfigurationProductRepository.save(pcConfigurationProduct);
    }

    @Override
    public void saveCustomConfig(PCConfiguration pcConfiguration) {
        pcConfigurationRepository.save(pcConfiguration);
    }

    @Override
    public List<PCConfiguration> getPCConfigMadeByUser(String username) {
        User user = userRepository.findUserByUsernameAndAuthorities_Authority(username, Role.USER);

        return pcConfigurationRepository.findByUser(user);
    }

    @Override
    public List<PCConfiguration> getAllPCConfigs() {
        return pcConfigurationRepository.findAll();
    }
}
