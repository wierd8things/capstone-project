package com.epam.pcconfig.repositories;

import com.epam.pcconfig.models.PCConfiguration;
import com.epam.pcconfig.models.User;
import com.epam.pcconfig.roles.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PCConfigurationRepository extends JpaRepository<PCConfiguration, Long> {
    @Query("SELECT pc FROM PCConfiguration pc WHERE pc.user.username = :username ORDER BY pc.createDate DESC LIMIT 1")
    PCConfiguration findLatestByUsername(@Param("username") String username);
    Optional<PCConfiguration> findByConfigIdAndUser_UserId(Long configId, Long sellerId);
    List<PCConfiguration> findByUser(User user);
    List<PCConfiguration> findAllByUser_Authorities_Authority(Role role);
}
