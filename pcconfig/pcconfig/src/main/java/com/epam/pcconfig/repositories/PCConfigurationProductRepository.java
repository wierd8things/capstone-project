package com.epam.pcconfig.repositories;

import com.epam.pcconfig.models.PCConfigurationProduct;
import com.epam.pcconfig.models.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PCConfigurationProductRepository extends JpaRepository<PCConfigurationProduct, Long> {
    List<PCConfigurationProduct> findPCConfigurationProductsByPcConfiguration_ConfigId(Long configId);

    List<PCConfigurationProduct> findProductByProduct_ProductTypeAndPcConfiguration_ConfigId(ProductType typeEnum, Long configId);
}
