document.addEventListener('DOMContentLoaded', () => {

    // Manage category visibility
    document.querySelectorAll('.side-category-icon').forEach(icon => {
        icon.addEventListener('click', () => {
            const category = icon.getAttribute('data-category');
            document.querySelectorAll('.components').forEach(comp => {
                comp.classList.remove('active');
            });
            document.getElementById(category).classList.add('active');
        });
    });

    // Controlling the disclosure of component parts
    document.querySelectorAll('.cpu-info-block-click').forEach(block => {
        const line = block.querySelector('.cpu-info-block-line');
        const details = block.querySelector('.cpu-info-block-details');

        line.addEventListener('click', () => {
            details.classList.toggle('active');
            line.classList.toggle('active');
        });
    });
});
