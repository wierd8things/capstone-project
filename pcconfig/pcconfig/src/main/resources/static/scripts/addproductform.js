document.addEventListener('DOMContentLoaded', (event) => {
    let attributeIndex = 0;

    document.querySelector('.add-attribute').addEventListener('click', () => {
        addAttribute();
    });

    function addAttribute() {
        // Получаем контейнер для атрибутов
        const attributesContainer = document.getElementById('attributes');

        // Создаем новую строку для атрибутов
        const newAttributeRow = document.createElement('div');
        newAttributeRow.classList.add('attribute');

        // Создаем поля для названия атрибута
        const attributeNameField = document.createElement('input');
        attributeNameField.type = 'text';
        attributeNameField.placeholder = 'Attribute';
        attributeNameField.name = `productDto.productAttributeDtoList[${attributeIndex}].attributeName`;
        attributeNameField.classList.add('input-field');
        attributeNameField.required = true;

        // Создаем поля для единицы измерения
        const measurementUnitField = document.createElement('input');
        measurementUnitField.type = 'text';
        measurementUnitField.placeholder = 'Unit';
        measurementUnitField.name = `productDto.productAttributeDtoList[${attributeIndex}].measurementUnit`;
        measurementUnitField.classList.add('input-field');

        // Создаем поля для значения
        const valueField = document.createElement('input');
        valueField.type = 'text';
        valueField.placeholder = 'Value';
        valueField.name = `productDto.productAttributeValueDtoList[${attributeIndex}].value`;
        valueField.classList.add('input-field');
        valueField.required = true;

        // Увеличиваем индекс для следующего атрибута
        attributeIndex++;

        // Добавляем все созданные поля в новую строку
        newAttributeRow.appendChild(attributeNameField);
        newAttributeRow.appendChild(measurementUnitField);
        newAttributeRow.appendChild(valueField);

        // Добавляем новую строку в контейнер
        attributesContainer.appendChild(newAttributeRow);
    }
});

function removeAttribute(button) {
    button.parentElement.remove();
}
