package com.epam.pcconfig.service;

import com.epam.pcconfig.models.Authority;
import com.epam.pcconfig.models.Store;
import com.epam.pcconfig.models.User;
import com.epam.pcconfig.models.dto.StoreDto;
import com.epam.pcconfig.models.dto.UserRegisterDto;
import com.epam.pcconfig.repositories.AuthorityRepository;
import com.epam.pcconfig.repositories.StoreRepository;
import com.epam.pcconfig.repositories.UserRepository;
import com.epam.pcconfig.roles.Role;
import com.epam.pcconfig.services.implementations.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class UserServiceTest {
    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private AuthorityRepository authorityRepository;

    @Mock
    private StoreRepository storeRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetUserByUsernameWithAuthorities() {
        //Given
        String username = "testUser";
        User user = new User();
        user.setUsername(username);
        Authority authority = new Authority();
        authority.setAuthority(Role.USER);
        authority.setUser(user);
        user.setAuthorities(List.of(authority));

        //When
        when(userRepository.findByUsernameWithAuthorities(username)).thenReturn(java.util.Optional.of(user));

        User result = userService.getUserByUsernameWithAuthorities(username);

        // Then
        assertEquals(username, result.getUsername());
        assertEquals(List.of(authority), result.getAuthorities());
    }

    @Test
    void testRegisterUser() {
        // Given
        UserRegisterDto userRegisterDto = new UserRegisterDto();
        userRegisterDto.setUsername("testUser");
        userRegisterDto.setPassword("password");
        userRegisterDto.setFirstName("John");
        userRegisterDto.setLastName("Doe");
        userRegisterDto.setEmail("test@example.com");

        when(passwordEncoder.encode(userRegisterDto.getPassword())).thenReturn("encodedPassword");

        // When
        userService.registerUser(userRegisterDto);

        // Then
        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        ArgumentCaptor<Authority> authorityCaptor = ArgumentCaptor.forClass(Authority.class);

        verify(userRepository, times(2)).save(userCaptor.capture());
        verify(authorityRepository, times(1)).save(authorityCaptor.capture());

        User capturedUser = userCaptor.getValue();
        Authority capturedAuthority = authorityCaptor.getValue();

        assertEquals("testUser", capturedUser.getUsername());
        assertEquals("John", capturedUser.getContactInfo().getFirst_name());
        assertEquals("Doe", capturedUser.getContactInfo().getLast_name());
        assertEquals("test@example.com", capturedUser.getContactInfo().getEmail());
        assertEquals("encodedPassword", capturedUser.getPassword());

        assertEquals(Role.USER, capturedAuthority.getAuthority());
        assertEquals(capturedUser, capturedAuthority.getUser());
    }

    @Test
    void testSwitchToSeller() {
        // Given
        String username = "testUser";
        StoreDto storeDto = new StoreDto();
        storeDto.setUsername(username);
        storeDto.setStoreName("Test Store");
        storeDto.setAddress("Test Address");
        storeDto.setContactEmail("store@example.com");
        storeDto.setStatus("active");

        List<Authority> authorities = new ArrayList<>();
        User user = new User();
        user.setUsername(username);
        Authority authority = new Authority();
        authority.setAuthority(Role.SELLER);
        authorities.add(authority);
        user.setAuthorities(authorities);
        when(userRepository.findByUsername(username)).thenReturn(user);

        // When
        userService.switchToSeller(storeDto);

        // Then
        verify(authorityRepository).save(any(Authority.class));
        verify(userRepository).save(any(User.class));
        verify(storeRepository).save(any(Store.class));
    }
}
