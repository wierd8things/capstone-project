package com.epam.pcconfig.service;

import com.epam.pcconfig.models.*;
import com.epam.pcconfig.models.dto.pcconfigs.PCConfigDto;
import com.epam.pcconfig.repositories.*;
import com.epam.pcconfig.roles.Role;
import com.epam.pcconfig.services.implementations.PCConfigurationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class PCConfigurationServiceTest {
    @InjectMocks
    private PCConfigurationServiceImpl pcConfigurationService;

    @Mock
    private PCConfigurationRepository pcConfigurationRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private ProductToStoreRepository productToStoreRepository;

    @Mock
    private PCConfigurationProductRepository pcConfigurationProductRepository;

    @Mock
    private ProductRepository productRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetPCConfigsWhereUserIsSeller() {
        //Given
        User seller = new User();
        Authority authority = new Authority();
        authority.setUser(seller);
        authority.setAuthority(Role.SELLER);
        seller.setAuthorities(List.of(authority));

        PCConfiguration pcConfiguration = new PCConfiguration();
        pcConfiguration.setUser(seller);

        List<PCConfiguration> pcConfigurations = List.of(pcConfiguration);

        //When
        when(pcConfigurationRepository.findAllByUser_Authorities_Authority(Role.SELLER)).thenReturn(pcConfigurations);

        List<PCConfiguration> result = pcConfigurationService.getPCConfigsWhereUserIsSeller(Role.SELLER);

        //Then
        assertEquals(pcConfigurations, result);
        assertEquals(Role.SELLER, result.getFirst().getUser().getAuthorities().getFirst().getAuthority());
    }

    @Test
    void testPostNewCustomPC() {
        // Given
        String username = "user1";
        PCConfigDto pcConfigDto = new PCConfigDto();
        pcConfigDto.setConfigName("Custom PC");

        User user = new User();
        user.setUsername(username);

        when(userRepository.findByUsername(username)).thenReturn(user);

        // When
        PCConfiguration result = pcConfigurationService.postNewCustomPC(pcConfigDto, username);

        // Then
        assertNotNull(result);
        assertEquals("Custom PC", result.getConfigName());
        assertEquals(user, result.getUser());
        verify(pcConfigurationRepository, times(1)).save(result);
    }

    @Test
    void testAssignProductToPc() {
        // Given
        String username = "user1";
        Long productId = 1L;

        User user = new User();
        user.setUserId(1L);
        user.setUsername(username);

        PCConfiguration pcConfiguration = new PCConfiguration();
        pcConfiguration.setConfigId(1L);
        pcConfiguration.setUser(user);
        pcConfiguration.setConfigName("TestPCName");
        pcConfiguration.setCreateDate(LocalDate.now(ZoneId.systemDefault()));

        Product product = new Product();
        product.setProductId(productId);

        PCConfigurationProduct pcConfigurationProduct = new PCConfigurationProduct();
        pcConfigurationProduct.setPcConfiguration(pcConfiguration);
        pcConfigurationProduct.setProduct(product);
        pcConfigurationProduct.setMinPrice(BigDecimal.TEN);

        when(pcConfigurationRepository.findLatestByUsername(username)).thenReturn(pcConfiguration);
        when(productRepository.findById(productId)).thenReturn(Optional.of(product));
        when(productToStoreRepository.findMinPriceByProductId(productId)).thenReturn(BigDecimal.TEN);
        when(pcConfigurationProductRepository.save(any(PCConfigurationProduct.class))).thenReturn(pcConfigurationProduct);

        // Argument captor
        ArgumentCaptor<PCConfigurationProduct> captor = ArgumentCaptor.forClass(PCConfigurationProduct.class);

        // When
        PCConfigurationProduct result = pcConfigurationService.assignProductToPc(username, productId);

        // Then
        verify(pcConfigurationProductRepository).save(captor.capture());
        PCConfigurationProduct captured = captor.getValue();

        assertEquals(pcConfiguration, captured.getPcConfiguration());
        assertEquals(product, captured.getProduct());
        assertEquals(BigDecimal.TEN, captured.getMinPrice());
        assertEquals(pcConfigurationProduct, result);
    }



    @Test
    void testSaveCustomConfig() {
        // Given
        PCConfiguration pcConfiguration = new PCConfiguration();

        // When
        pcConfigurationService.saveCustomConfig(pcConfiguration);

        // Then
        verify(pcConfigurationRepository, times(1)).save(pcConfiguration);
    }

    @Test
    void testGetPCConfigMadeByUser() {
        // Given
        String username = "user1";
        User user = new User();
        user.setUsername(username);

        PCConfiguration pcConfiguration = new PCConfiguration();
        pcConfiguration.setUser(user);

        List<PCConfiguration> pcConfigurations = List.of(pcConfiguration);

        when(userRepository.findUserByUsernameAndAuthorities_Authority(username, Role.USER)).thenReturn(user);
        when(pcConfigurationRepository.findByUser(user)).thenReturn(pcConfigurations);

        // When
        List<PCConfiguration> result = pcConfigurationService.getPCConfigMadeByUser(username);

        // Then
        assertEquals(pcConfigurations, result);
    }

    @Test
    void testGetAllPCConfigs() {
        // Given
        PCConfiguration pcConfiguration1 = new PCConfiguration();
        PCConfiguration pcConfiguration2 = new PCConfiguration();

        List<PCConfiguration> pcConfigurations = List.of(pcConfiguration1, pcConfiguration2);

        when(pcConfigurationRepository.findAll()).thenReturn(pcConfigurations);

        // When
        List<PCConfiguration> result = pcConfigurationService.getAllPCConfigs();

        // Then
        assertEquals(pcConfigurations, result);
    }
}
