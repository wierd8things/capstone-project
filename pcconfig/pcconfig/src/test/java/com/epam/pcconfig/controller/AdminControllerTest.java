package com.epam.pcconfig.controller;

import com.epam.pcconfig.models.Product;
import com.epam.pcconfig.models.ProductAttribute;
import com.epam.pcconfig.models.dto.products.MeasureUnitEnum;
import com.epam.pcconfig.models.dto.products.ProductTypeEnum;
import com.epam.pcconfig.repositories.ProductAttributeRepository;
import com.epam.pcconfig.repositories.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class AdminControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductAttributeRepository productAttributeRepository;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void testAddProduct() throws Exception {
        mockMvc.perform(post("/admin/newproduct")
                        .param("name", "Test Product")
                        .param("productType", String.valueOf(ProductTypeEnum.CPU)))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/addattributes"));
    }

    @Test
    void testAddProductAttribute() throws Exception {
        mockMvc.perform(post("/admin/newattributes")
                        .param("attributeName", "USB ports")
                        .param("measurementUnit", String.valueOf(MeasureUnitEnum.PCS)))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/attributesobserve"));
    }

    @Test
    void testAddProductValues() throws Exception {
        // Add a product first
        mockMvc.perform(post("/admin/newproduct")
                        .param("name", "Test Product")
                        .param("productType", String.valueOf(ProductTypeEnum.CPU)))
                .andExpect(status().is3xxRedirection());

        // Retrieve the created product to get its ID
        Product createdProduct = productRepository.findProductByProductName("Test Product");
        Long productId = createdProduct.getProductId();

        // Add an attribute
        mockMvc.perform(post("/admin/newattributes")
                        .param("attributeName", "USB slots")
                        .param("measurementUnit", String.valueOf(MeasureUnitEnum.PCS)))
                .andExpect(status().is3xxRedirection());

        // Retrieve the created attribute to get its ID
        ProductAttribute createdAttribute = productAttributeRepository.findProductAttributeByAttributeName("USB slots");
        Long attributeId = createdAttribute.getProductAttributeId();

        // Add a value for the attribute
        mockMvc.perform(post("/admin/newattributevalues")
                        .param("productId", String.valueOf(productId))
                        .param("productAttributeId", String.valueOf(attributeId))
                        .param("value", "3.5"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/admin/valuesobserve"));
    }
}
