package com.epam.pcconfig.service;

import com.epam.pcconfig.models.PCConfiguration;
import com.epam.pcconfig.models.PCConfigurationProduct;
import com.epam.pcconfig.models.Product;
import com.epam.pcconfig.models.ProductType;
import com.epam.pcconfig.models.dto.products.ProductTypeEnum;
import com.epam.pcconfig.repositories.PCConfigurationProductRepository;
import com.epam.pcconfig.services.implementations.PCConfigurationProductServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class PCConfigurationProductServiceTest {

    @InjectMocks
    private PCConfigurationProductServiceImpl pcConfigurationProductService;

    @Mock
    private PCConfigurationProductRepository pcConfigurationProductRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAllProductsByConfigId() {
        //Given
        Long configId = 1L;
        PCConfiguration pcConfiguration = new PCConfiguration();
        pcConfiguration.setConfigId(configId);

        PCConfigurationProduct pcConfigurationProduct = new PCConfigurationProduct();
        pcConfigurationProduct.setPcConfiguration(pcConfiguration);

        List<PCConfigurationProduct> pcConfigurationProducts = List.of(pcConfigurationProduct);

        //When
        when(pcConfigurationProductRepository.findPCConfigurationProductsByPcConfiguration_ConfigId(configId)).thenReturn(pcConfigurationProducts);

        List<PCConfigurationProduct> result = pcConfigurationProductService.getAllProductsByConfigId(configId);

        //Then
        assertEquals(pcConfigurationProducts, result);
        assertEquals(configId, result.getFirst().getPcConfiguration().getConfigId());
    }

    @Test
    void testGetAllProductsByConfigIdAndProductType() {
        //Given
        Long configId = 1L;
        PCConfiguration pcConfiguration = new PCConfiguration();
        pcConfiguration.setConfigId(configId);

        Product product = new Product();
        ProductType productType = new ProductType();
        productType.setProductTypeName(ProductTypeEnum.CPU);
        product.setProductType(productType);

        PCConfigurationProduct pcConfigurationProduct = new PCConfigurationProduct();
        pcConfigurationProduct.setPcConfiguration(pcConfiguration);
        pcConfigurationProduct.setProduct(product);

        List<PCConfigurationProduct> pcConfigurationProducts = List.of(pcConfigurationProduct);

        //When
        when(pcConfigurationProductRepository.findProductByProduct_ProductTypeAndPcConfiguration_ConfigId(productType, configId)).thenReturn(pcConfigurationProducts);

        List<PCConfigurationProduct> result = pcConfigurationProductService.getAllProductsByConfigIdAndProductType(productType, configId);

        //Then
        assertEquals(result, pcConfigurationProducts);
        assertEquals(configId, result.getFirst().getPcConfiguration().getConfigId());
        assertEquals(productType, result.getFirst().getProduct().getProductType());
    }
}
