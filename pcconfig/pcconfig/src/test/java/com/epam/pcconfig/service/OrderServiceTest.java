package com.epam.pcconfig.service;

import com.epam.pcconfig.models.Cart;
import com.epam.pcconfig.models.Order;
import com.epam.pcconfig.models.ShippingTypeEnum;
import com.epam.pcconfig.models.User;
import com.epam.pcconfig.models.dto.orders.OrderDto;
import com.epam.pcconfig.models.dto.orders.StatusEnum;
import com.epam.pcconfig.models.embedded.FullAddress;
import com.epam.pcconfig.repositories.CartRepository;
import com.epam.pcconfig.repositories.OrderRepository;
import com.epam.pcconfig.repositories.UserRepository;
import com.epam.pcconfig.services.implementations.OrderServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OrderServiceTest {
    @InjectMocks
    private OrderServiceImpl orderService;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private CartRepository cartRepository;

    @Mock
    private UserRepository userRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void addCartToOrderTest() {
        //Given
        String username = "TestUserName";

        User user = new User();
        user.setUsername(username);

        Cart cart = new Cart();
        cart.setTotalPrice(new BigDecimal("1500.00"));

        OrderDto orderDto = new OrderDto();
        orderDto.setFullAddress(new FullAddress("Tajikistan", "Dushanbe", "Shotemura st."));
        orderDto.setShippingType(ShippingTypeEnum.COURIER);
        orderDto.setPostelCode(123456);

        //When
        when(userRepository.findByUsername(username)).thenReturn(user);
        when(cartRepository.findByUserUsername(username)).thenReturn(cart);

        orderService.addCartToOrder(username, orderDto);

        ArgumentCaptor<Order> orderCaptor = ArgumentCaptor.forClass(Order.class);
        verify(orderRepository).save(orderCaptor.capture());

        Order capturedOrder = orderCaptor.getValue();

        //Then
        assertEquals(user, capturedOrder.getUser());
        assertEquals(cart, capturedOrder.getCart());
        assertEquals(LocalDate.now(ZoneId.systemDefault()), capturedOrder.getPlaceOrderDate());
        assertEquals(StatusEnum.ON_THE_WAY, capturedOrder.getStatus());
        assertEquals(orderDto.getFullAddress(), capturedOrder.getFullAddress());
        assertEquals(orderDto.getShippingType(), capturedOrder.getShippingType());
        assertEquals(orderDto.getPostelCode(), capturedOrder.getPostelCode());
        assertEquals(cart.getTotalPrice(), capturedOrder.getTotalPrice());
    }

    @Test
    void getOrdersByUserId() {
        //Given
        String username = "TestUserName";
        User user = new User();
        user.setUsername(username);

        Order order = new Order();
        order.setUser(user);

        //When
        when(userRepository.findByUsername(username)).thenReturn(user);
        when(orderRepository.findByUserUsername(username)).thenReturn(order);

        Order capturedOrder = orderService.getOrdersByUserId(username);

        //Then
        assertEquals(user, capturedOrder.getUser());
    }
}
