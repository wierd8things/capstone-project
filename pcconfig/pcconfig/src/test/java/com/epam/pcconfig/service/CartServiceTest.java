package com.epam.pcconfig.service;

import com.epam.pcconfig.models.Cart;
import com.epam.pcconfig.models.PCConfiguration;
import com.epam.pcconfig.models.PCConfigurationProduct;
import com.epam.pcconfig.models.User;
import com.epam.pcconfig.repositories.CartRepository;
import com.epam.pcconfig.repositories.PCConfigurationProductRepository;
import com.epam.pcconfig.repositories.PCConfigurationRepository;
import com.epam.pcconfig.repositories.UserRepository;
import com.epam.pcconfig.services.implementations.CartServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CartServiceTest {
    @InjectMocks
    private CartServiceImpl cartService;

    @Mock
    private CartRepository cartRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PCConfigurationRepository pcConfigurationRepository;

    @Mock
    private PCConfigurationProductRepository pcConfigurationProductRepository;


    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testAddConfigToCart() {
        //Given
        String username = "testuser";

        User user = new User();
        user.setUsername(username);

        PCConfiguration pcConfiguration = new PCConfiguration();
        pcConfiguration.setConfigId(1L);

        PCConfigurationProduct product1 = new PCConfigurationProduct();
        product1.setMinPrice(new BigDecimal("100.00"));

        PCConfigurationProduct product2 = new PCConfigurationProduct();
        product2.setMinPrice(new BigDecimal("150.00"));

        List<PCConfigurationProduct> products = Arrays.asList(product1, product2);

        //When
        when(userRepository.findByUsername(username)).thenReturn(user);
        when(pcConfigurationRepository.findLatestByUsername(username)).thenReturn(pcConfiguration);
        when(pcConfigurationProductRepository.findPCConfigurationProductsByPcConfiguration_ConfigId(pcConfiguration.getConfigId())).thenReturn(products);

        cartService.addPCConfigToCart(username);

        ArgumentCaptor<Cart> cartCaptor = ArgumentCaptor.forClass(Cart.class);
        verify(cartRepository).save(cartCaptor.capture());

        Cart capturedCart = cartCaptor.getValue();

        //Then
        assertEquals(user, capturedCart.getUser());
        assertEquals(pcConfiguration, capturedCart.getPcConfiguration());
        assertEquals(LocalDate.now(ZoneId.systemDefault()), capturedCart.getAddToCartDate());
        assertEquals(new BigDecimal("250.00"), capturedCart.getTotalPrice());
    }
}
