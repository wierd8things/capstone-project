package com.epam.pcconfig.controller;

import com.epam.pcconfig.models.User;
import com.epam.pcconfig.models.dto.UserRegisterDto;
import com.epam.pcconfig.models.embedded.ContactInfo;
import com.epam.pcconfig.services.implementations.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class SignInControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserServiceImpl userService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void testRegisterUserAccountSuccess() throws Exception {
        // Given
        UserRegisterDto userRegisterDto = new UserRegisterDto();
        userRegisterDto.setEmail("test@example.com");
        userRegisterDto.setPassword("password");

        // When/Then
        mockMvc.perform(MockMvcRequestBuilders.post("/signin/save")
                        .contentType("application/x-www-form-urlencoded")
                        .param("email", userRegisterDto.getEmail())
                        .param("password", userRegisterDto.getPassword()))
                .andExpect(status().isOk())
                .andExpect(view().name("/authorized/success"));
    }

    @Test
    void testRegisterUserAccountDuplicateEmail() throws Exception {
        // Given
        User existingUser = new User();
        existingUser.setContactInfo(new ContactInfo());
        existingUser.getContactInfo().setEmail("test@example.com");

        UserRegisterDto userRegisterDto = new UserRegisterDto();
        userRegisterDto.setEmail("test@example.com");
        userRegisterDto.setPassword("password");

        // Mock userService behavior to return existing user
        when(userService.findByUserEmail(Mockito.anyString())).thenReturn(existingUser);

        // When/Then
        mockMvc.perform(MockMvcRequestBuilders.post("/signin/save")
                        .contentType("application/x-www-form-urlencoded")
                        .param("email", userRegisterDto.getEmail())
                        .param("password", userRegisterDto.getPassword()))
                .andExpect(status().isOk())
                .andExpect(model().attributeHasFieldErrors("user", "email"))
                .andExpect(model().attributeExists("user"))
                .andExpect(view().name("signorlogin/signin"));
    }
}
