package com.epam.pcconfig.service;

import com.epam.pcconfig.models.Product;
import com.epam.pcconfig.models.ProductToStore;
import com.epam.pcconfig.models.ProductType;
import com.epam.pcconfig.models.dto.products.ProductTypeEnum;
import com.epam.pcconfig.repositories.ProductToStoreRepository;
import com.epam.pcconfig.services.implementations.ProductToStoreServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class ProductToStoreTest {
    @InjectMocks
    private ProductToStoreServiceImpl productToStoreService;

    @Mock
    private ProductToStoreRepository productToStoreRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetProductsByTypeOrderByPrice() {
        //Given
        ProductType productType = new ProductType();
        productType.setProductTypeName(ProductTypeEnum.CPU);

        ProductType productType1 = new ProductType();
        productType.setProductTypeName(ProductTypeEnum.GPU);

        Product product1 = new Product();
        ProductToStore productToStore1 = new ProductToStore();
        product1.setProductType(productType1);
        productToStore1.setProduct(product1);

        Product product2 = new Product();
        ProductToStore productToStore2 = new ProductToStore();
        product2.setProductType(productType);
        productToStore2.setProduct(product2);

        Product product3 = new Product();
        ProductToStore productToStore3 = new ProductToStore();
        product3.setProductType(productType);
        productToStore3.setProduct(product3);

        Product product4 = new Product();
        ProductToStore productToStore4 = new ProductToStore();
        product4.setProductType(productType1);
        productToStore4.setProduct(product4);

        Product product5 = new Product();
        ProductToStore productToStore5 = new ProductToStore();
        product5.setProductType(productType);
        productToStore5.setProduct(product5);

        List<ProductToStore> productToStoreList = List.of(productToStore2, productToStore3, productToStore5);

        //When
        when(productToStoreRepository.findByProduct_ProductType(productType)).thenReturn(productToStoreList);
        List<ProductToStore> result = productToStoreService.getProductsByTypeOrderByPrice(productType);

        //Then
        assertEquals(productToStoreList, result);
    }
}
