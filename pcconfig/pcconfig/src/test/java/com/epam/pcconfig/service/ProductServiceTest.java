package com.epam.pcconfig.service;

import com.epam.pcconfig.models.*;
import com.epam.pcconfig.models.dto.products.*;
import com.epam.pcconfig.repositories.*;
import com.epam.pcconfig.services.implementations.ProductServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ProductServiceTest {
    @InjectMocks
    private ProductServiceImpl productService;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductAttributeRepository productAttributeRepository;

    @Mock
    private StoreRepository storeRepository;

    @Mock
    private ProductToStoreRepository productToStoreRepository;

    @Mock
    private ProductAttributeValueRepository productAttributeValueRepository;

    @Mock
    private ProductTypeRepository productTypeRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAllProductsByTypeName() {
        // Given
        ProductTypeEnum productType = ProductTypeEnum.CPU;
        Product product = new Product();
        List<Product> products = List.of(product);

        // When
        when(productRepository.findProductByProductType_ProductTypeName(productType)).thenReturn(products);
        List<Product> result = productService.getAllProductsByTypeName(productType);

        // Then
        assertEquals(products, result);
    }

    @Test
    void testGetProductsByTypeAndSortedByPrice() {
        // Given
        Long productTypeId = 1L;
        Product product = new Product();
        List<Product> products = List.of(product);

        // When
        when(productRepository.findByProductTypeIdOrderByPrice(productTypeId)).thenReturn(products);
        List<Product> result = productService.getProductsByTypeAndSortedByPrice(productTypeId);

        // Then
        assertEquals(products, result);
    }

    @Test
    void testGetAllProducts() {
        // Given
        Product product = new Product();
        List<Product> products = List.of(product);

        // When
        when(productRepository.findAll()).thenReturn(products);
        List<Product> result = productService.getAllProducts();

        // Then
        assertEquals(products, result);
    }

    @Test
    void testGetAllProductsBySeller() {
        // Given
        String seller = "seller";
        Store store = new Store();
        store.setStoreId(1L);
        Product product = new Product();
        List<Product> products = List.of(product);

        // When
        when(storeRepository.findStoreByUserUsername(seller)).thenReturn(store);
        when(productToStoreRepository.findByStoreId(store.getStoreId())).thenReturn(products);
        List<Product> result = productService.getAllProductsBySeller(seller);

        // Then
        assertEquals(products, result);
    }

    @Test
    void testAddProduct() {
        // Given
        ProductDto productDto = new ProductDto();
        productDto.setName("New Product");
        productDto.setProductType(ProductTypeEnum.CPU);

        ProductType productType = new ProductType();
        productType.setProductTypeName(ProductTypeEnum.CPU);

        Product product = new Product();
        product.setProductName("New Product");
        product.setProductType(productType);

        // When
        when(productTypeRepository.findByProductTypeName(ProductTypeEnum.CPU)).thenReturn(Optional.of(productType));
        when(productRepository.save(any(Product.class))).thenReturn(product);
        Product result = productService.addProduct(productDto);

        // Then
        assertEquals("New Product", result.getProductName());
        assertEquals(productType, result.getProductType());
        verify(productRepository, times(1)).save(any(Product.class));
    }

    @Test
    void testAddProductAttribute() {
        // Given
        ProductAttributeDto productAttributeDto = new ProductAttributeDto();
        productAttributeDto.setAttributeName("Speed");
        productAttributeDto.setMeasurementUnit(MeasureUnitEnum.GHZ);

        ProductAttribute productAttribute = new ProductAttribute();
        productAttribute.setAttributeName("Speed");
        productAttribute.setMeasurementUnit(MeasureUnitEnum.GHZ);

        // When
        when(productAttributeRepository.save(any(ProductAttribute.class))).thenReturn(productAttribute);
        ProductAttribute result = productService.addProductAttribute(productAttributeDto);

        // Then
        assertEquals("Speed", result.getAttributeName());
        assertEquals(MeasureUnitEnum.GHZ, result.getMeasurementUnit());
        verify(productAttributeRepository, times(1)).save(any(ProductAttribute.class));
    }

    @Test
    void testAddProductAttributeValue() {
        // Given
        ProductAttributeValueDto productAttributeValueDto = new ProductAttributeValueDto();
        productAttributeValueDto.setProductId(1L);
        productAttributeValueDto.setProductAttributeId(1L);
        productAttributeValueDto.setValue("3.5");

        Product product = new Product();
        product.setProductId(1L);

        ProductAttribute productAttribute = new ProductAttribute();
        productAttribute.setProductAttributeId(1L);

        ProductAttributeValue productAttributeValue = new ProductAttributeValue();
        productAttributeValue.setProduct(product);
        productAttributeValue.setProductAttribute(productAttribute);
        productAttributeValue.setValue("3.5");

        // When
        when(productRepository.findById(1L)).thenReturn(Optional.of(product));
        when(productAttributeRepository.findById(1L)).thenReturn(Optional.of(productAttribute));
        when(productAttributeValueRepository.save(any(ProductAttributeValue.class))).thenReturn(productAttributeValue);
        ProductAttributeValue result = productService.addProductAttributeValue(productAttributeValueDto);

        // Then
        assertEquals(product, result.getProduct());
        assertEquals(productAttribute, result.getProductAttribute());
        assertEquals("3.5", result.getValue());
        verify(productAttributeValueRepository, times(1)).save(any(ProductAttributeValue.class));
    }

    @Test
    void testAddProductToStore() {
        // Given
        ProductToStoreDto productToStoreDto = new ProductToStoreDto();
        productToStoreDto.setProductId(1L);
        productToStoreDto.setPrice(BigDecimal.TEN);

        Product product = new Product();
        product.setProductId(1L);

        Store store = new Store();
        store.setStoreId(1L);

        ProductToStore productToStore = new ProductToStore();
        productToStore.setStore(store);
        productToStore.setProduct(product);
        productToStore.setStorePrice(BigDecimal.TEN);

        // When
        when(productRepository.findById(1L)).thenReturn(Optional.of(product));
        when(storeRepository.findStoreByUserUsername("seller")).thenReturn(store);
        productService.addProductToStore(productToStoreDto, "seller");

        // Then
        verify(productToStoreRepository, times(1)).save(any(ProductToStore.class));
    }

    @Test
    void testFindAllProducts() {
        // Given
        Product product = new Product();
        List<Product> products = List.of(product);

        // When
        when(productRepository.findAll()).thenReturn(products);
        List<Product> result = productService.findAllProducts();

        // Then
        assertEquals(products, result);
    }

    @Test
    void testFindAllAttributes() {
        // Given
        ProductAttribute productAttribute = new ProductAttribute();
        List<ProductAttribute> productAttributes = List.of(productAttribute);

        // When
        when(productAttributeRepository.findAll()).thenReturn(productAttributes);
        List<ProductAttribute> result = productService.findAllAttributes();

        // Then
        assertEquals(productAttributes, result);
    }

    @Test
    void testGetProductById() {
        // Given
        Long productId = 1L;
        Product product = new Product();
        product.setProductId(productId);

        // When
        when(productRepository.findById(productId)).thenReturn(Optional.of(product));
        Product result = productService.getProductById(productId);

        // Then
        assertEquals(product, result);
    }

    @Test
    void testGetAllProductValuesByProductId() {
        // Given
        Long productId = 1L;
        ProductAttributeValue productAttributeValue = new ProductAttributeValue();
        List<ProductAttributeValue> productAttributeValues = List.of(productAttributeValue);

        // When
        when(productAttributeValueRepository.findProductAttributeValueByProduct_ProductId(productId)).thenReturn(productAttributeValues);
        List<ProductAttributeValue> result = productService.getAllProductValuesByProductId(productId);

        // Then
        assertEquals(productAttributeValues, result);
    }

    @Test
    void testGetAllProductValues() {
        // Given
        ProductAttributeValue productAttributeValue = new ProductAttributeValue();
        List<ProductAttributeValue> productAttributeValues = List.of(productAttributeValue);

        // When
        when(productAttributeValueRepository.findAll()).thenReturn(productAttributeValues);
        List<ProductAttributeValue> result = productService.getAllProductValues();

        // Then
        assertEquals(productAttributeValues, result);
    }
}
